lexer grammar MKSONLexer;

@lexer::header {
package antlrgen;
}

KeywordImport: 'import';
KeywordModule: 'module';
KeywordScript: 'script';
KeywordAs: 'as';
KeywordIf: 'if';
KeywordIn: 'in';
KeywordNotIn: '!in';
KeywordElse: 'else';
KeywordTrue: 'true';
KeywordFalse: 'false';
KeywordNull: 'null';

Comment : '/*' (Comment | .)*? '*/' -> channel(HIDDEN);
LineComment  : '//' .*? '\n' -> channel(HIDDEN);

Reference: ReferenceIndicator ID;
ReferenceIndicator: ModuleReferenceIndicator | SelfReferenceIndicator;
ModuleReferenceIndicator: '@';
SelfReferenceIndicator: '$'+ '+'*;
ID: [_a-zA-Z] [_a-zA-Z0-9]*;

String: Quote (ESC | ~["\\])* Quote;
fragment ESC: '\\' (["\\/bfnrt$] | UNICODE);
fragment UNICODE: 'u' HEX HEX HEX HEX;
fragment HEX: [0-9a-fA-F];

Number: HEXINT | (INT '.' [0-9] + EXP? | INT EXP | INT);
fragment INT: '0' | [1-9] [0-9]*;
fragment HEXINT: '#' [0-9a-fA-F]+;
fragment EXP: [Ee] [+\-]? INT;

Dot: '.';
Asterisk: '*';
OpenBrace: '{';
ClosedBrace: '}';
OpenBracket: '[';
ClosedBracket: ']';
OpenParen: '(';
ClosedParen: ')';
ExclamationMark: '!';
Dash: '-';
Caret: '^';
Slash: '/';
Backslash: '\\';
Modulus: '%';
Plus: '+';
Equals: '==';
NotEquals: '!=';
GreaterThan: '>';
LessThan: '<';
GreaterThanOrEqual: '>=';
LessThanOrEqual: '<=';
And: '&&';
Or: '||';
Comma: ',';
Assignment: '=';
Colon: ':';
DoubleColon: '::';
Quote: '"';
QuestionMark: '?';

WS: [ \t\n\r]+ -> channel(HIDDEN);

