parser grammar MKSONParser;

@parser::header {
package antlrgen;
}

options { tokenVocab=MKSONLexer; }

mkson: meta* value meta*;

/*
 * Meta Values
 */
meta: importStatement
    | directiveStatement
    | scriptStatement;

importStatement: 'import' '{' ((path ('as' alias)? ',')* (path ('as' alias)?) (',')?)? '}';
directiveStatement: 'module' '{' ((directive ',')* directive (',')?)? '}';
scriptStatement: 'script' '{' ((path ('as' alias)? ',')* (path ('as' alias)?) (',')?)? '}';
path: String;
alias: ID;
directive: ID;

/*
 * Real Values
 */
value: string
     | number
     | bool
     | nulltype
     | undefined
     | object
     | inheritance '::' object
     | array
     | reference
     | value '.' functionCall
     | value '.' ID
     | value '[' value ']'
     | functionCall
     | '(' value ')'
     | ifStatement
     | '!' value
     | '-' value
     | value '^' value
     | value '*' value
     | value ('/' | '\\' | '%') value
     | value ('+' | '-') value
     | value ('in' | '!in') value
     | value ('==' | '!=' | '>=' | '<=' | '>' | '<') value
     | value '&&' value
     | value '||' value;

ifStatement: 'if' ('(' value ')' ('{' value '}' | value))+ 'else' ('{' value '}' | value);
reference: Reference | ReferenceIndicator '[' value ']';
functionCall: (ID '::')? ID '(' (value ',')* value? ')';
inheritance: '{' value '}';
object: '{' ((pair ',')* pair (',')?)? '}';
pair: Asterisk? (ID | String) (':' | '=') value;
array: '[' ((value ',')* value (',')?)? ']';
string: String;
number: Number;
bool: ('true' | 'false');
nulltype: 'null';
undefined: '?';
