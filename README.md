# @MKSON

This project is no longer in development. It served its purpose to further my education. However, the code is still here 
and this compiler has all the features I wished to put in and more! The documentation below is unfinished. To see 
some what the language is like take a look at the test files in the src/test folder. * Jake Ploskey

## DOCUMENTATION

MKSON is a superset of JSON that compiles to JSON. It is designed to facilitate generation and reuse of complex and 
interconnected sets of JSON data. 

---

### Purpose

The purpose of MKSON is to generate JSON. While JSON is suited to it's original purpose as a human *readable* and 
omnipresent data storage format, it is not ideal for being *written* by humans beyond a limited level of complexity. 

Once that level of complexity is passed, data becomes repetitive, interconnected and loses readability:

* Some objects inevitably want to contain other objects.
* Some objects are essentially the same as others with one or two tweaked fields. 
* Some objects need to have specific fields that might be forgotten.
* Some objects have fields could ideally be omitted and specified with a default value.
* Some values would be better stored in constants or local variables and referenced from elsewhere.
* Some values are simply transformations of other values.


MKSON seeks to:
 
1. Ensure data reuse and correctness through in-file and cross-file references, inheritance and temporary values.
2. Allow data transformation via operators and functions. 
3. Allow easier reading and writing through less restrictive syntax and comments.

---

### Under Construction 

As MKSON nears completion this documentation will too. If you would like to get a feel for the language, take a look 
at the tests in the source folder. To try it out, run a gradle build in this project's root directory. You'll find a 
runnable jar file *mkson.jar* in the generated *compiled/libs* directory. 

To compile a single .mkson file (possibly with references to other files), run: 

    java -jar mkson.jar <input-file>.mkson <output-file>.json
    
To compile all .mkson files in a directory into an output directory, run:

    java -jar mkson.jar <input-directory> <output-directory>
    
*The input directory can be a hierarchy. All .mkson files will be compiled into a matching hierarchy in the output 
folder as .json files.*


---

# Documentation

### Syntax

MKSON is a superset of JSON, therefore all JSON is valid MKSON. However, there are a few niceties that have been added to 
base syntax:

```
/*
 * Look! A block comment! 
 */
 
person: {                       // Keys do not need to be quoted unless they contain spaces.
    name: "John Doe",            
    height: 5.2,
    eyes: #0000FF,              // Hex notation can be used to specify integer values.
    enjoyments: [ 
        "biking",
        "data reusability",      // Trailing commas are allowed in both objects and arrays.
    ]
}
```
    
### Types and Operations 

---

#### Boolean and If

*Operators*
```
&& -> and
|| -> or
!  -> not
```
*Functions*
```
str(boolean) : Return the string representation of the boolean.
```
*Example*
```
[
    true && false,               // == false 
    true || false == true,       // == true 
    !true                        // == false
]
```
        
#### Truthyness
The following non-boolean values are considered falsy when evaluated by boolean operators and if statements:

* Empty Objects
* Empty Arrays
* Empty Strings
* Zero

*Example*
```
[
    ![],                      // == true 
    !{},                      // == true 
    !"",                      // == true 
    !0,                       // == true 
    [] && [ 1, 2, 3 ],        // == false
    "A" || {}                 // == true
]
```

If statements in MKSON return a value based on a triggered predicate. For if statements, braces are always optional,
parenthesis around predicates are required and non-boolean values used as predicates are evaluated by their truthyness.

```
[
    if (true) 1 else 2,                // == 1
    
    if ([]) 1 else if ({}) 0 else 2,   // == 2 
    
    if (false) {                       // == 3
        2 
    } else { 
        3 
    },                                 
    
    
    if (0) {                           // == 4
        -4
    } else if ([ 1, 2, 3 ]) {
        4
    } else {
        false
    }                                 
]
```
        
#### Number

*Operators*
```
+  : addition
-  : subtraction or negation
*  : multiplication 
/  : division 
\  : integer division
%  : modular division
^  : exponentiation
<  : less than
>  : greater than
<= : less than or equal
>= : greater than or equal
```

*Functions*
```
number.round() : Round a number to the nearest integer value.
number.abs()   : Return the absolute value of a number.
number.str()   : Return the string representation of a number.
```

*Example*
```
[
    2 + 2 % 2,           // == 0,
    4.5 / 2,             // == 2.25
    4.5 \ 2,             // == 2
    2 ^ 4,               // == 16
    2 * -2,              // == -4 
    round(4.6),          // == 5
    abs(-2),             // == 2
    -2.abs()             // == -2
]
```

#### String

*Operators*
```
+  : concatenation
[] : indexing
```

*Functions*
```
string.length()                          : Return the length of a string.
string.slice(start: number, end: number) : Return the substring of a string from a start index an end index. The end index is inclusive.
string.slice(start: number)              : Return the substring of a string from a start index to the end of the string. 
string.upper()                           : Convert all alphabetical characters to upper-case.
string.lower()                           : Convert all alphabetical characters in lower-case.
string.replace(old: string, new: string) : Replace all instances of a substring with a new string.
string.split()                           : Splits a string into an array by whitespace.
string.split(delimeter: string)          : Splits a string into an array by a given delimeter.
string.first()                           : Returns the first character of a string.
string.last()                            : Returns the last character of a string.
string.trim()                            : Removes all leading and trailing whitespace. 
string.matches(regex: string)            : Returns true if a string matches a provided java-style regex. 
string.repeat(repetitions: number)       : Concatenates a string with itself a given number of times.
string.str()                             : Returns the string representation of the a string.
```

*Example*
```
```

#### Array

*Operators*
```
```

Built-In Functions
```
```

#### Object
