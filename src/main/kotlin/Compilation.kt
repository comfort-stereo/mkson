
import antlrgen.MKSONParser.*
import extensions.*
import org.antlr.v4.runtime.Parser
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.misc.Interval
import org.antlr.v4.runtime.tree.ParseTree
import org.antlr.v4.runtime.tree.ParseTreeVisitor
import org.antlr.v4.runtime.tree.TerminalNode
import kotlin.collections.*

fun buildModule(module: Module, external: List<Module>) {
    ModuleBuilder(module, external).build()
}

private class ModuleBuilder(val module: Module, external: List<Module>) {
    private val importMap = module.generateImportMap(external)
    private val scriptMap = module.generateScriptMap()
    private val referencesPassed = mutableSetOf<ReferenceContext>()
    private val resolutionCache = mutableMapOf<ValueContext, MksonValue>()
    private val inheritanceCache = mutableMapOf<ObjectContext, MksonObject?>()
    private lateinit var top: ParseTree

    init {
        for (script in module.meta.scripts) ScriptManager.register(script)
    }

    fun build() {
        Compiler.alertProcessingModule(module)
        module.finalize(resolveMkson(module.tree))
        Compiler.alertFinishedProcessing()
    }

    private fun resolveMkson(node: MksonContext): MksonValue {
        Compiler.alertProcessingNode(node)
        val value = node.children.find { child -> child is ValueContext } as ValueContext
        val child = value.getChild(0)
        if (child is ObjectContext) {
            child.pairs.find { pair -> pair.key in importMap }?.let { pair ->
                throw MksonTopNameConflictError(pair.key)
            }
        }
        top = child
        return resolveValue(value)
    }

    private fun resolveValue(node: ValueContext, useCache: Boolean = false, copyCached: Boolean = true): MksonValue {
        if (useCache) resolutionCache[node]?.let { return if (copyCached) it.copy() else it }

        Compiler.alertProcessingNode(node)
        node.ensureChildrenAreNotBorrowed()

        val value = when (node.type) {
            ValueContextType.Value -> resolve(node.getChild(0))
            ValueContextType.BinaryOperation -> resolveOperation(node, isUnary = false)
            ValueContextType.UnaryOperation -> resolveOperation(node, isUnary = true)
            ValueContextType.Parenthesis -> resolveValue(node.getChild(1) as ValueContext)
        }

        if (useCache) resolutionCache[node] = value
        Compiler.alertProcessingNode(node)
        return value
    }

    private fun resolveObject(node: ObjectContext): MksonObject {

        fun injectRecompiledPairs(node: ObjectContext, ancestor: MksonObject) {
            val keys = node.pairs.mapTo(mutableSetOf(), PairContext::key)
            for ((key, tree) in ancestor.recompilationMap) {
                if (key in keys) continue
                val value = PseudoValueContext(tree)
                val pair = PairContext(node, -1).apply {
                    if (key in ancestor.transientKeys) addChild(PseudoTerminalNode("*"))
                    addChild(PseudoTerminalNode(key))
                    addChild(PseudoTerminalNode("="))
                    addChild(value)
                }
                value.parent = pair
                node.addChild(pair)
            }
        }

        return MksonObject().apply {
            inheritance(node)?.let { ancestor ->
                inherit(ancestor)
                injectRecompiledPairs(node, ancestor)
            }

            if (node.isVacant) return this

            for (pair in node.pairs) {
                val key = pair.key
                val value = pair.value
                if (pair.isTransient) transientKeys.add(key) else transientKeys.remove(key)
                if (pair.isRecompiled) recompilationMap[key] = value
                this[key] = resolveValue(value, useCache = true)
            }
            for (pair in node.pairs) resolutionCache.remove(pair.value)

            inheritanceCache.remove(node)
        }
    }

    private fun resolveArray(node: ArrayContext): MksonArray {
        return MksonArray().apply {
            if (node.isVacant) return this
            for (value in node.values) add(resolveValue(value, useCache = true))
            for (value in node.values) resolutionCache.remove(value)
        }
    }

    private fun resolveReference(node: ReferenceContext): MksonValue {

        fun objectGet(node: ObjectContext, key: String, inheritanceDepth: Int = 0): MksonValue? {
            if (inheritanceDepth == 0) {
                node.get(key)?.let { value ->
                    return resolveValue(value, useCache = true, copyCached = false)
                }
                return inheritance(node)?.get(key)
            }
            return inheritance(node)?.get(key, ancestor = inheritanceDepth - 1)
        }

        fun arrayGet(node: ArrayContext, index: Int): MksonValue? {
            return node.get(index)?.let { cached ->
                resolveValue(cached, useCache = true, copyCached = false)
            }
        }

        fun resolveTopValue(reference: String, accessor: MksonValue?): MksonValue {
            val key = if (accessor == null) reference.substring(1) else {
                if (accessor.type == MKSON_UNDEFINED) return MksonUndefined
                accessor.toString()
            }

            val top = top

            var value = when (top) {
                is ObjectContext -> {
                    if (accessor != null && accessor.type != MKSON_STRING)
                        throw MksonInvalidBinaryOperationError("[]", MKSON_OBJECT_TYPENAME, accessor.typeName)
                    objectGet(top, key)
                }
                is ArrayContext -> {
                    if (accessor == null) throw MksonReferenceDNEError(reference)
                    if (accessor.type != MKSON_NUMBER)
                        throw MksonInvalidBinaryOperationError("[]", MKSON_ARRAY_TYPENAME, accessor.typeName)
                    val index = key.toInt()
                    arrayGet(top, index)
                }
                else -> null
            }

            if (value == null) {
                value = importMap[key]?.output ?:
                    throw MksonReferenceDNEError(reference + if (accessor == null) "" else "[${accessor.toJSONString()}]")
            }

            return value
        }

        fun resolveSelfValue(reference: String, accessor: MksonValue?): MksonValue {
            val key = if (accessor == null) reference.substringAfterLast("$").substringAfterLast("+") else {
                if (accessor.type == MKSON_UNDEFINED) return MksonUndefined
                if (accessor.type != MKSON_STRING && accessor.type != MKSON_NUMBER) {
                    val topTypeName = if (top is ObjectContext) MKSON_OBJECT_TYPENAME else MKSON_ARRAY_TYPENAME
                    throw MksonInvalidBinaryOperationError("[]", topTypeName, accessor.typeName)
                }
                accessor.toString()
            }

            val ancestor = reference.count { it == '+' }
            val occurrence = reference.count { it == '$' }
            var occurrences = 0

            if (accessor != null && accessor is MksonNumber) {
                if (ancestor != 0) throw MksonAttemptToAccessArrayAncestorError()

                var current = node.parentArray
                while (current != null) {
                    val value = arrayGet(current, accessor.value.toInt())
                    if (++occurrences == occurrence) return value ?: break
                    current = current.parentArray
                }

                throw MksonSelfReferenceDNEError(reference + "[${accessor.toJSONString()}]")
            }

            var current = node.parentObject
            while (current != null) {
                val value = objectGet(current, key, ancestor)
                if (value != null && ++occurrences == occurrence) return value
                current = current.parentObject
            }

            throw MksonSelfReferenceDNEError(reference)
        }

        referencesPassed.add(node) || throw MksonCircularReferenceError(node.text)

        val reference = node.getChild(0).text
        val self = reference.startsWith("$")

        val accessor = if (node.childCount > 1) {
            resolveValue(node.getChild(2) as ValueContext)
        } else {
            null
        }

        val value = if (self) {
            resolveSelfValue(reference, accessor)
        } else {
            resolveTopValue(reference, accessor)
        }

        referencesPassed.remove(node)

        return value.apply { isReferenced = true }
    }

    private fun resolveOperation(node: ValueContext, isUnary: Boolean): MksonValue {
        if (isUnary) {
            val operator = node.getChild(0).text
            val operand = resolveValue(node.getChild(1) as ValueContext)
            return operand.operate(operator)
                ?: throw MksonInvalidUnaryOperationError(operator, operand.typeName)
        }

        val operator = node.getChild(1).text

        if (operator == ".") {
            val right = node.getChild(2)
            return if (right is TerminalNode) {
                if (node.getChild(0) is UndefinedContext) return MksonUndefined
                val left = resolveValue(node.getChild(0) as ValueContext) as? MksonObject
                val key = right.text
                left?.get(key) ?: throw MksonInvalidPathError(key)
            } else {
                resolveFunctionDotCall(node)
            }
        }

        if (operator == "::") return resolveObject(node.getChild(2) as ObjectContext)

        val left = resolveValue(node.getChild(0) as ValueContext)
        val right = resolveValue(node.getChild(2) as ValueContext)
        return left.operate(operator, right) ?: throw MksonInvalidBinaryOperationError(
            if (operator == "[") "[]" else operator, left.typeName, right.typeName
        )
    }

    private fun resolveIfStatement(node: IfStatementContext): MksonValue {
        val children = node.children.filterIsInstance<ValueContext>()
        for ((i, child) in children.withIndex()) {
            if (i % 2 != 0) continue
            if (i == children.lastIndex) break

            val value = resolveValue(child)
            if (value.type == MKSON_UNDEFINED) return MksonUndefined
            if (value.isTruthy()) return resolveValue(children[i + 1])
        }
        return resolveValue(children.last())
    }

    private fun callCustomFunction(namespace: String, function: String, arguments: List<MksonValue>): MksonValue {
        val script = scriptMap[namespace] ?: throw MksonScriptDNEError(namespace)
        return ScriptManager.call(script, function, arguments)
    }

    private fun resolveFunctionCall(node: FunctionCallContext): MksonValue {
        val arguments = node.children.filterIsInstance<ValueContext>().map { resolveValue(it) }

        val child = node.getChild(1)
        if (child is TerminalNode && child.text == "::") {
            val namespace = node.getChild(0).text
            val function = node.getChild(2).text
            return callCustomFunction(namespace, function, arguments)
        }

        val function = node.getChild(0).text
        return callBuiltInFunction(function, arguments)
    }

    private fun resolveFunctionDotCall(node: ValueContext): MksonValue {
        val call = node.getChild(2) as FunctionCallContext

        val reciever = resolveValue(node.getChild(0) as ValueContext)
        val tailing = call.children.asSequence().filterIsInstance<ValueContext>().map { resolveValue(it) }
        val arguments = mutableListOf(reciever).apply { addAll(tailing) }

        val child = call.getChild(1)
        if (child is TerminalNode && child.text == "::") {
            val namespace = call.getChild(0).text
            val function = call.getChild(2).text
            return callCustomFunction(namespace, function, arguments)
        }
        
        val function = call.getChild(0).text
        return callBuiltInFunction(function, arguments)
    }

    private fun resolve(node: ParseTree): MksonValue {
        val rule = (node as ParserRuleContext).ruleIndex
        return when (rule) {
            RULE_string -> resolveString(node as StringContext)
            RULE_number -> resolveNumber(node as NumberContext)
            RULE_bool -> resolveBool(node as BoolContext)
            RULE_object -> resolveObject(node as ObjectContext)
            RULE_array -> resolveArray(node as ArrayContext)
            RULE_reference -> resolveReference(node as ReferenceContext)
            RULE_value -> resolveValue(node as ValueContext)
            RULE_mkson -> resolveMkson(node as MksonContext)
            RULE_ifStatement -> resolveIfStatement(node as IfStatementContext)
            RULE_functionCall -> resolveFunctionCall(node as FunctionCallContext)
            RULE_nulltype -> MksonNull
            RULE_undefined -> MksonUndefined
            else -> throw MksonShouldNotHappenError()
        }
    }

    private fun inheritance(node: ObjectContext): MksonObject? {
        inheritanceCache[node]?.let { ancestor -> return ancestor }
        val parent = node.parent ?: return null
        val ancestor = if (parent.childCount == 3 && parent.getChild(1).text == "::") {
            val inheritance = parent.getChild(0) as InheritanceContext
            resolveValue(inheritance.value) as? MksonObject
                ?: throw MksonInheritanceFromNonObjectValueError(inheritance.value.text)
        } else {
            null
        }
        inheritanceCache[node] = ancestor
        return ancestor
    }

    companion object {
        private fun resolveString(node: StringContext): MksonString {
            return MksonString(node.getChild(0).text.stripQuotes())
        }

        private fun resolveNumber(node: NumberContext): MksonNumber {
            val text = node.getChild(0).text
            return if (text.startsWith("#")) {
                MksonNumber(Integer.parseInt(text.substringAfter("#"), 16).toDouble())
            } else {
                MksonNumber(text.toDouble())
            }
        }

        private fun resolveBool(node: BoolContext): MksonBoolean {
            val text = node.getChild(0).text
            return MksonBoolean(text.toBoolean())
        }
    }
}

private class PseudoTerminalNode(text: String) : TerminalNode {
    private val string: String = text

    override fun getText(): String = string
    override fun getParent(): ParseTree = throw UnsupportedOperationException()
    override fun getChild(i: Int): ParseTree = throw UnsupportedOperationException()
    override fun getSymbol(): Token = throw UnsupportedOperationException()
    override fun getPayload(): Any = throw UnsupportedOperationException()
    override fun getChildCount(): Int = throw UnsupportedOperationException()
    override fun toStringTree(parser: Parser?): String = throw UnsupportedOperationException()
    override fun toStringTree(): String = throw UnsupportedOperationException()
    override fun getSourceInterval(): Interval = throw UnsupportedOperationException()
    override fun <T : Any?> accept(visitor: ParseTreeVisitor<out T>?): T = throw UnsupportedOperationException()
}

private class PseudoValueContext(redirect: ValueContext) : ValueContext(redirect.parent as ParserRuleContext, -1) {
    init {
        children = redirect.children
    }
}
