package extensions

import java.util.regex.Pattern

/*
 * Missing \f equivalent and \u (unicode) equivalent.
 */
private val ESCAPE_TO_REAL = arrayOf(
    "\\\\" to "\\",
    "\\\"" to "\"",
    "\\/"  to "/",
    "\\n"  to "\n",
    "\\t"  to "\t",
    "\\b"  to "\b",
    "\\r"  to "\r"
)

fun StringBuilder.stripQuotes() {
    if (startsWith('\"') && endsWith('\"')) {
        deleteCharAt(lastIndex)
        deleteCharAt(0)
    }
}

fun StringBuilder.unescapeSpecialCharacters() {
    for ((from, to) in ESCAPE_TO_REAL) replaceAll(from, to)
}

fun StringBuilder.escapeSpecialCharacters() {
    for ((from, to) in ESCAPE_TO_REAL) replaceAll(to, from)
}

fun StringBuilder.replaceAll(substring: String, replacement: String) {
    val pattern = Pattern.compile(Pattern.quote(substring))
    val matcher = pattern.matcher(this)
    var start = 0
    while (matcher.find(start)) {
        replace(matcher.start(), matcher.end(), replacement)
        start = matcher.start() + replacement.length
    }
}

