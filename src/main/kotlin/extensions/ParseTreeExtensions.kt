package extensions

import antlrgen.MKSONParser.*
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTree
import org.antlr.v4.runtime.tree.TerminalNode

fun ParseTree.getNthParentObject(n: Int): ObjectContext? {
    if (n < 1) throw IllegalArgumentException()
    var node = this
    var count = 0
    while (count < n) {
        node = node?.parent ?: return null
        count++
    }
    return node as ObjectContext
}

val ParseTree.parentObject: ObjectContext?
    get () {
        var node: ParseTree? = this
        do { node = node?.parent ?: return null } while (node !is ObjectContext)
        return node
    }

val ParseTree.parentArray: ArrayContext?
    get () {
        var node: ParseTree? = this
        do { node = node?.parent ?: return null } while (node !is ArrayContext)
        return node
    }

val ParseTree.parentKey: String?
    get() {
        var node: ParseTree? = this
        do { node = node?.parent ?: return null } while (node !is PairContext)
        return node.getChild(0).text.stripQuotes()
    }

val ParseTree.parentKeyAndContainer: Pair<String, ParserRuleContext>?
    get() {
        var previous: ParseTree? = null
        var node: ParseTree? = this

        while (node !is ObjectContext && node !is ArrayContext || previous == null) {
            previous = node ?: return null
            node = node.parent
        }
        if (node is ObjectContext) {
            val key = "'${(previous as PairContext).key}'"
            return key to node
        }
        if (node is ArrayContext) {
            for ((i, child) in node.values.withIndex()) {
                if (child == previous) return "[$i]" to node
            }
        }
        return null
    }

fun ParseTree.trace(): String {
    var node = this
    return mutableListOf<String>().apply {
        while (true) {
            val (key, next) = node.parentKeyAndContainer ?: break
            node = next
            add(key)
        }
    }.asReversed().joinToString(" -> ")
}

enum class ValueContextType { Value, BinaryOperation, UnaryOperation, Parenthesis }

val ValueContext.type: ValueContextType
    get() {
        return if (childCount > 1) {
            val first = getChild(0)
            if (first is TerminalNode && first.text == "(") {
                ValueContextType.Parenthesis
            } else if (childCount == 2) {
                ValueContextType.UnaryOperation
            } else {
                ValueContextType.BinaryOperation
            }
        } else {
            ValueContextType.Value
        }
    }

fun ValueContext.ensureChildrenAreNotBorrowed() {
    if (children.any { it is ParserRuleContext && it.parent == this }) return
    for (child in children) {
        (child as? ParserRuleContext)?.parent = this
    }
}

val InheritanceContext.value: ValueContext
    get() = children[1] as ValueContext

fun ObjectContext.get(key: String): ValueContext? = pairsReversed.find { it.key == key }?.value

val ObjectContext.pairs: Sequence<PairContext>
    get() = children.asSequence().filterIsInstance<PairContext>()
val ObjectContext.pairsReversed: Sequence<PairContext>
    get() = children.asReversed().asSequence().filterIsInstance<PairContext>()

val ObjectContext.isVacant: Boolean
    get() = children.all { it !is PairContext }

fun ArrayContext.get(index: Int): ValueContext? {
    for ((i, value) in values.withIndex()) {
        if (index == i) return value
    }
    return null
}

val PairContext.key: String
    get() = getChild(if (childCount == 4) 1 else 0).text.stripQuotes()
val PairContext.value: ValueContext
    get() = getChild(childCount - 1) as ValueContext
val PairContext.isTransient: Boolean
    get() = getChild(0).text == "*"
val PairContext.isRecompiled: Boolean
    get() {
        val child = getChild(childCount - 2)
        return child.text == "="
    }

val ArrayContext.values: Sequence<ValueContext>
    get() = children.asSequence().filterIsInstance<ValueContext>()
val ArrayContext.valuesReversed: Sequence<ValueContext>
    get() = children.asReversed().asSequence().filterIsInstance<ValueContext>()

val ArrayContext.isVacant: Boolean get() = children.all { it !is ValueContext }
