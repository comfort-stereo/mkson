package extensions

fun <T> MutableCollection<T>.addOrRemove(element: T, add: Boolean): Boolean {
    return if (add) add(element) else remove(element)
}

