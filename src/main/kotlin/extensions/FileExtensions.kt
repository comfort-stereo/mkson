package extensions

import java.io.File
import java.nio.file.Files
import java.nio.file.InvalidPathException
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes

val File.canonicalFileOrNull: File? get() = if (isFile) canonicalFile else null
fun File.canonicalFileOrElse(block: (File) -> File): File = if (isFile) canonicalFile else block(this)

val File.hasValidPath: Boolean
    get() = try {
        Paths.get(path)
        true
    } catch (exception: InvalidPathException) {
        false
    }

val File.inode: Int
    get() {
        val attributes = Files.readAttributes(Paths.get(this.path), BasicFileAttributes::class.java)
        val fileKey = attributes.fileKey().toString()
        val inode = fileKey.substring(fileKey.indexOf("ino=") + 4, fileKey.indexOf(")")).toInt()
        return inode
    }

fun File.section(lineNumber: Int, columnNumber: Int, height: Int): String {
    val start = lineNumber - height
    return buildString {
        useLines { lines ->
            for ((i, line) in lines.withIndex()) {
                if (i < start) continue
                append(line)
                append('\n')
                if (i == lineNumber - 1) break
            }
        }
        append("- ".repeat(columnNumber / 2), "^")
    }
}
