package extensions

import java.io.File

infix fun String.slash(other: String): String = this + File.separator + other

fun String.isIntString(): Boolean {
    return try {
        toInt()
        true
    } catch (exception: NumberFormatException) {
        false
    }
}

fun String.ensureSuffix(suffix: String): String {
    return if (endsWith(suffix)) {
        this
    } else {
        this + suffix
    }
}

fun String.replaceSuffix(suffix: String, replacement: String): String {
    return if (endsWith(suffix)) {
        removeSuffix(suffix) + replacement
    } else {
        this
    }
}

fun String.stripQuotes(): String {
    return if (startsWith('\"') && endsWith('\"')) {
        return substring(1, lastIndex)
    } else {
        this
    }
}

fun String.remove(substring: String): String = replace(substring, "")
fun String.toCanonicalFile(): File = File(this).canonicalFile
fun String.toCanonicalFileOrNull(): File? = File(this).canonicalFileOrNull
fun String.toCanonicalFileOrElse(block: (File) -> File): File = File(this).canonicalFileOrElse(block)
val String.isAbsolutePath: Boolean get() = File(this).isAbsolute
