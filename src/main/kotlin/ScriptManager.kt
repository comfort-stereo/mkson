
import jdk.nashorn.internal.runtime.ParserException
import java.io.FileReader
import javax.script.Invocable
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.ScriptException

object ScriptManager {

    private val evaluator by lazy { createEngine() }
    private val engines = mutableMapOf<String, Invocable>() // This maps script paths to engines.

    fun createEngine(): ScriptEngine {
        return ScriptEngineManager().getEngineByName("javascript") ?: throw MksonScriptingNotAvailableError()
    }

    fun register(script: Script) {
        val path = script.file.path
        if (path in engines) return

        val engine = createEngine()

        try {
            engine.eval(FileReader(path))
        } catch (exception: ScriptException) {
            throw MksonScriptNotParsableError(script)
        }

        engines[path] = engine as Invocable
    }

    fun evaluate(js: String): Any? {
        return evaluator.eval("($js)") // Parenthesis are required.
    }

    fun call(script: Script, function: String, arguments: List<MksonValue>): MksonValue {
        if (arguments.any { it.type == MKSON_UNDEFINED }) return MksonUndefined
        val engine = engines[script.file.path] ?: throw MksonShouldNotHappenError()
        val jsArguments = arguments.map { evaluate(it.toJSONString()) }.toTypedArray()

        val result = try {
            engine.invokeFunction(function, *jsArguments)
        } catch (exception: ParserException) {
            // TODO: Handle other stuff.
            throw MksonUnresolvedCustomFunctionError(script.alias, function, arguments)
        }

        return MksonValue.fromGeneralValue(result)
    }
}
