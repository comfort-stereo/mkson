import extensions.replaceAll
import java.util.regex.PatternSyntaxException
import kotlin.reflect.KFunction1

typealias BuiltInFunction = KFunction1<@ParameterName(name = "arguments") List<MksonValue>, MksonValue>

fun callBuiltInFunction(name: String, arguments: List<MksonValue>): MksonValue {
    if (arguments.any { it.type == MKSON_UNDEFINED }) return MksonUndefined
    val signature = Signature(name, arguments.map { it.type })
    val function = BUILT_IN_FUNCTION_MAP[signature]

    /*
     * Special case because there technically aren't any varargs.
     */
    if (function == null) {
        if (name == "format" && arguments.firstOrNull()?.type == MKSON_STRING) {
            return stringFormat(arguments)
        }
        throw MksonUnresolvedBuiltInFunctionError(name, arguments)
    }

    return function(arguments)
}

private fun signature(function: BuiltInFunction): String {

    /*
     * JANKY!: The reflection library is not included, so function.name should probably not work. However, it
     * returns the name of the function anyway with some warning text. Equality between two function references
     * gives a null reference exception and this is a work around. Identity equality does not for this work either,
     * meaning the function references don't technically point to the same object.
     */

    fun name(function: BuiltInFunction): String {
        return function.name.substringAfter("function ").substringBefore(" (")
    }

    val name = name(function)
    if (name == "arrayInsert") return "insert(any)"
    if (name.endsWith("ToString")) return "str(any)"

    for ((signature, value) in BUILT_IN_FUNCTION_MAP) {
        if (value.name == function.name) {
            return "${signature.name}(${signature.types.joinToString(transform = ::typeToTypeName)})"
        }
    }
    throw MksonShouldNotHappenError()
}

private data class Signature(val name: String, val types: List<Int>)

private val BUILT_IN_FUNCTION_MAP = mapOf(
    Signature("keys",       listOf(MKSON_OBJECT))                               to ::objectKeys,
    Signature("values",     listOf(MKSON_OBJECT))                               to ::objectValues,
    Signature("items",      listOf(MKSON_OBJECT))                               to ::objectItems,
    Signature("length",     listOf(MKSON_ARRAY))                                to ::arrayLength,
    Signature("slice",      listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_NUMBER))    to ::arraySlice,
    Signature("slice",      listOf(MKSON_ARRAY, MKSON_NUMBER))                  to ::arraySliceBeyond,
    Signature("first",      listOf(MKSON_ARRAY))                                to ::arrayFirst,
    Signature("last",       listOf(MKSON_ARRAY))                                to ::arrayLast,
    Signature("join",       listOf(MKSON_ARRAY, MKSON_STRING))                  to ::arrayJoin,
    Signature("sum",        listOf(MKSON_ARRAY))                                to ::arraySum,
    Signature("all",        listOf(MKSON_ARRAY))                                to ::arrayAll,
    Signature("any",        listOf(MKSON_ARRAY))                                to ::arrayAny,
    Signature("none",       listOf(MKSON_ARRAY))                                to ::arrayNone,
    Signature("unique",     listOf(MKSON_ARRAY))                                to ::arrayUnique,
    Signature("repeat",     listOf(MKSON_ARRAY, MKSON_NUMBER))                  to ::arrayRepeat,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_OBJECT))    to ::arrayInsert,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_ARRAY))     to ::arrayInsert,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_STRING))    to ::arrayInsert,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_NUMBER))    to ::arrayInsert,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_BOOLEAN))   to ::arrayInsert,
    Signature("insert",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_NULL))      to ::arrayInsert,
    Signature("inject",     listOf(MKSON_ARRAY, MKSON_NUMBER, MKSON_ARRAY))     to ::arrayInject,
    Signature("length",     listOf(MKSON_STRING))                               to ::stringLength,
    Signature("slice",      listOf(MKSON_STRING, MKSON_NUMBER, MKSON_NUMBER))   to ::stringSlice,
    Signature("slice",      listOf(MKSON_STRING, MKSON_NUMBER))                 to ::stringSliceBeyond,
    Signature("upper",      listOf(MKSON_STRING))                               to ::stringUpper,
    Signature("lower",      listOf(MKSON_STRING))                               to ::stringLower,
    Signature("replace",    listOf(MKSON_STRING, MKSON_STRING, MKSON_STRING))   to ::stringReplace,
    Signature("split",      listOf(MKSON_STRING, MKSON_STRING))                 to ::stringSplit,
    Signature("split",      listOf(MKSON_STRING))                               to ::stringSplitSpace,
    Signature("first",      listOf(MKSON_STRING))                               to ::stringFirst,
    Signature("last",       listOf(MKSON_STRING))                               to ::stringLast,
    Signature("trim",       listOf(MKSON_STRING))                               to ::stringTrim,
    Signature("matches",    listOf(MKSON_STRING, MKSON_STRING))                 to ::stringMatches,
    Signature("repeat",     listOf(MKSON_STRING, MKSON_NUMBER))                 to ::stringRepeat,
    Signature("abs",        listOf(MKSON_NUMBER))                               to ::numberAbs,
    Signature("round",      listOf(MKSON_NUMBER))                               to ::numberRound,
    Signature("str",        listOf(MKSON_OBJECT))                               to ::objectToString,
    Signature("str",        listOf(MKSON_ARRAY))                                to ::arraytoString,
    Signature("str",        listOf(MKSON_STRING))                               to ::stringToString,
    Signature("str",        listOf(MKSON_NUMBER))                               to ::numberToString,
    Signature("str",        listOf(MKSON_BOOLEAN))                              to ::booleanToString,
    Signature("str",        listOf(MKSON_NULL))                                 to ::nullToString
)

private fun objectKeys(arguments: List<MksonValue>): MksonArray {
    val obj = arguments[0] as MksonObject
    return MksonArray(obj.size).apply {
        for (key in obj.keys) add(MksonString(key))
    }
}

private fun objectValues(arguments: List<MksonValue>): MksonArray {
    val obj = arguments[0] as MksonObject
    return MksonArray(obj.size).apply {
        for (value in obj.values) add(value)
    }
}

private fun objectItems(arguments: List<MksonValue>): MksonArray {
    val obj = arguments[0] as MksonObject
    return MksonArray(obj.size).apply {
        for ((key, value) in obj.entries) {
            val item = MksonArray(2).apply {
                add(MksonString(key))
                add(value)
            }
            add(item)
        }
    }
}

private fun objectToString(arguments: List<MksonValue>): MksonString {
    val obj = arguments[0] as MksonObject
    return MksonString(obj.toJSONString())
}

private fun arrayLength(arguments: List<MksonValue>): MksonValue {
    val array = arguments[0] as MksonArray
    return MksonNumber(array.size)
}

private fun arraySlice(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    val start = (arguments[1] as MksonNumber).value.toInt()
    val end = (arguments[2] as MksonNumber).value.toInt()

    if (start < 0) throw MksonInvalidFunctionArgumentsError(signature(::arraySlice), "Negative start index.")
    if (start > end) throw MksonInvalidFunctionArgumentsError(signature(::arraySlice), "Start index '$start' is greater than end index '$end'.")
    if (end > array.lastIndex) throw MksonInvalidFunctionArgumentsError(signature(::arraySlice), "End index is out of bounds.")

    return MksonArray(end - start).apply {
        for (i in start..end) add(array[i])
    }
}

private fun arraySliceBeyond(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    val start = (arguments[1] as MksonNumber).value.toInt()
    val end = array.lastIndex

    if (start < 0 || start > array.lastIndex) throw MksonInvalidFunctionArgumentsError(signature(::arraySliceBeyond), "Start index '$start' is out of bounds.")

    return MksonArray(end - start).apply {
        for (i in start..end) add(array[i])
    }
}

private fun arrayFirst(arguments: List<MksonValue>): MksonValue {
    val array = arguments[0] as MksonArray
    return array.getOrElse(0) { throw MksonInvalidFunctionArgumentsError(signature(::arrayFirst), "Array is empty.") }
}

private fun arrayLast(arguments: List<MksonValue>): MksonValue {
    val array = arguments[0] as MksonArray
    return array.getOrElse(array.lastIndex) { throw MksonInvalidFunctionArgumentsError(signature(::arrayLast), "Array is empty.") }
}

private fun arrayJoin(arguments: List<MksonValue>): MksonString {
    val array = arguments[0] as MksonArray
    val separator = (arguments[1] as MksonString).value

    if (array.isEmpty()) return MksonString()

    return MksonString().apply {
        val builder = value
        for ((i, value) in array.withIndex()) {
            val string = value as? MksonString
                ?: throw MksonInvalidFunctionArgumentsError(signature(::arrayJoin), "Element '$value' at index '$i' is not a string.")
            builder.append(string)
            builder.append(separator)
        }
        builder.replace(builder.length - separator.length, builder.length, "")
    }
}

private fun arraySum(arguments: List<MksonValue>): MksonNumber {
    val array = arguments[0] as MksonArray
    var i = 0
    val sum = array.sumByDouble { value ->
        val number = value as? MksonNumber ?:
            throw MksonInvalidFunctionArgumentsError(signature(::arraySum), "Element '$value' at index '$i' is not a number.")
        i++
        number.value
    }
    return MksonNumber(sum)
}

private fun arrayAll(arguments: List<MksonValue>): MksonBoolean {
    val array = arguments[0] as MksonArray
    return array.all { it.isTruthy() }.let(::MksonBoolean)
}

private fun arrayNone(arguments: List<MksonValue>): MksonBoolean {
    val array = arguments[0] as MksonArray
    return array.none { it.isTruthy() }.let(::MksonBoolean)
}

private fun arrayAny(arguments: List<MksonValue>): MksonBoolean {
    val array = arguments[0] as MksonArray
    return array.any { it.isTruthy() }.let(::MksonBoolean)
}

private fun arrayUnique(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    return array.asSequence().distinct().mapTo(MksonArray()) { it }
}

private fun arrayRepeat(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    val times = (arguments[1] as MksonNumber).value.toInt()

    if (times < 0) throw MksonInvalidFunctionArgumentsError(signature(::arrayRepeat), "Given negative number of repeats.")

    return MksonArray(array.size * times).apply {
        for (i in 1..times) addAll(array)
    }
}

private fun arrayInsert(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    val index = (arguments[1] as MksonNumber).value.toInt()
    val inserted = arguments[2]

    if (index < 0 || index > array.size) throw MksonInvalidFunctionArgumentsError(signature(::arrayInsert), "Index $index is out of bounds on array of size ${array.size}.")

    return array.applyCopy { add(index, inserted) }
}

private fun arrayInject(arguments: List<MksonValue>): MksonArray {
    val array = arguments[0] as MksonArray
    val index = (arguments[1] as MksonNumber).value.toInt()
    val injected = arguments[2] as MksonArray

    if (index < 0 || index > array.size) throw MksonInvalidFunctionArgumentsError(signature(::arrayInject), "Index $index is out of bounds on array of size ${array.size}.")

    return array.applyCopy { addAll(index, injected) }
}

private fun arraytoString(arguments: List<MksonValue>): MksonString {
    val array = arguments[0] as MksonArray
    return MksonString(array.toJSONString())
}

private fun stringLength(arguments: List<MksonValue>): MksonValue {
    val string = arguments[0] as MksonString
    return MksonNumber(string.value.length)
}

private fun stringSlice(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    val start = (arguments[1] as MksonNumber).value.toInt()
    val end = (arguments[2] as MksonNumber).value.toInt()

    if (start < 0) throw MksonInvalidFunctionArgumentsError(signature(::stringSlice), "Negative start index.")
    if (start > end) throw MksonInvalidFunctionArgumentsError(signature(::stringSlice), "Start index '$start' is greater than end index '$end'.")
    if (end > string.value.lastIndex) throw MksonInvalidFunctionArgumentsError(signature(::stringSlice), "End index is out of bounds.")

    return MksonString(end - start).apply {
        for (i in start..end) value.append(string.value[i])
    }
}

private fun stringSliceBeyond(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    val start = (arguments[1] as MksonNumber).value.toInt()
    val end = string.value.lastIndex

    if (start < 0 || start > end) throw MksonInvalidFunctionArgumentsError(signature(::arraySliceBeyond), "Start index '$start' is out of bounds.")

    return MksonString(end - start).apply {
        for (i in start..end) value.append(string.value[i])
    }
}

private fun stringUpper(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    return string.applyCopy {
        for ((i, character) in value.withIndex()) value[i] = character.toUpperCase()
    }
}

private fun stringLower(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    return string.applyCopy {
        for ((i, character) in value.withIndex()) value[i] = character.toLowerCase()
    }
}

private fun stringReplace(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    val substring = (arguments[1] as MksonString).value.toString()
    val replacement = (arguments[2] as MksonString).value.toString()
    return string.applyCopy { value.replaceAll(substring, replacement) }
}

private fun stringSplit(arguments: List<MksonValue>): MksonArray {
    val string = arguments[0] as MksonString
    val delimeter = (arguments[1] as MksonString).value.toString()
    val split = string.value.toString().splitToSequence(delimeter)
    return MksonArray().apply {
        for (substring in split) add(MksonString(substring))
    }
}

private fun stringSplitSpace(arguments: List<MksonValue>): MksonArray {
    val string = arguments[0] as MksonString
    val split = string.value.toString().splitToSequence(" ")
    return MksonArray().apply {
        for (substring in split) add(MksonString(substring))
    }
}

private fun stringFirst(arguments: List<MksonValue>): MksonString {
    val string = (arguments[0] as MksonString).value
    val character = string.getOrElse(0) { throw MksonInvalidFunctionArgumentsError(signature(::arrayFirst), "String is empty.") }
    return MksonString(character.toString())
}

private fun stringLast(arguments: List<MksonValue>): MksonString {
    val string = (arguments[0] as MksonString).value
    val character = string.getOrElse(string.lastIndex) { throw MksonInvalidFunctionArgumentsError(signature(::arrayFirst), "String is empty.") }
    return MksonString(character.toString())
}

private fun stringTrim(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    return MksonString(string.value.trim() as String)
}

private fun stringMatches(arguments: List<MksonValue>): MksonBoolean {
    val string = (arguments[0] as MksonString).value
    val regex = try {
        Regex((arguments[1] as MksonString).value.toString())
    } catch (exception: PatternSyntaxException) {
        throw MksonInvalidFunctionArgumentsError(signature(::stringMatches), "Regex pattern is invalid.")
    }
    return MksonBoolean(string.matches(regex))
}

private fun stringRepeat(arguments: List<MksonValue>): MksonString {
    val string = (arguments[0] as MksonString).value
    val times = (arguments[1] as MksonNumber).value.toInt()

    if (times < 0) throw MksonInvalidFunctionArgumentsError(signature(::stringRepeat), "Given negative number of repeats.")

    return MksonString(string.length * times).apply {
        for (i in 1..times) value.append(string)
    }
}

private fun stringFormat(arguments: List<MksonValue>): MksonString {
    val string = (arguments[0] as MksonString).value
    var position = 1
    val result = buildString {
        for ((i, character) in string.withIndex()) {
            if (character == '%') {
                if (i != 0 && string[i - 1] == '\\') {
                    this[lastIndex] = '%'
                    continue
                }
                val interpolated = arguments.getOrNull(position++)
                if (interpolated != null) {
                    append(interpolated.toString())
                    continue
                }
            }
            append(character)
        }
    }
    return MksonString(result)
}

private fun stringToString(arguments: List<MksonValue>): MksonString {
    val string = arguments[0] as MksonString
    return string
}

private fun numberAbs(arguments: List<MksonValue>): MksonNumber {
    val number = arguments[0] as MksonNumber
    return number.applyCopy { value = Math.abs(value) }
}

private fun numberRound(arguments: List<MksonValue>): MksonNumber {
    val number = arguments[0] as MksonNumber
    return number.applyCopy { value = Math.round(value).toDouble() }
}

private fun numberToString(arguments: List<MksonValue>): MksonString {
    val number = arguments[0] as MksonNumber
    return MksonString(number.toString())
}

private fun booleanToString(arguments: List<MksonValue>): MksonString {
    val boolean = arguments[0] as MksonBoolean
    return MksonString(boolean.toJSONString())
}

private fun nullToString(arguments: List<MksonValue>): MksonString {
    val nulltype = arguments[0] as MksonNull
    return MksonString(nulltype.toJSONString())
}

