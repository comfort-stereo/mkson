
import antlrgen.MKSONLexer
import antlrgen.MKSONParser
import extensions.section
import org.antlr.v4.runtime.*
import java.io.File
import java.io.FileInputStream

class MKSONErrorListener(val module: File) : BaseErrorListener() {
    override fun syntaxError(recognizer: Recognizer<*, *>, offender: Any?, line: Int, column: Int, msg: String?,
                             exception: RecognitionException?) {
        val symbol = offender.toString().substringAfter('=').substringBefore(',')
        val section = module.section(line, column, 10)
        throw MksonSyntaxError(module, line, column, symbol, section)
    }
}

fun parseMksonFile(file: File): MKSONParser.MksonContext {
    val lexer = MKSONLexer(ANTLRInputStream(FileInputStream(file)))
    val parser = MKSONParser(CommonTokenStream(lexer))
    parser.removeErrorListeners()
    parser.addErrorListener(MKSONErrorListener(file))
    return parser.mkson()
}

fun parseMksonValue(string: String, module: File): MKSONParser.ValueContext {
    val lexer = MKSONLexer(ANTLRInputStream(string))
    val stream = CommonTokenStream(lexer)
    val parser = MKSONParser(stream)
    parser.removeErrorListeners()
    parser.addErrorListener(MKSONErrorListener(module))
    return parser.value()
}
