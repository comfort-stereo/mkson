
import utilities.forEachParallel
import utilities.parallelStreamOf
import java.io.File

fun retrieveModules(): List<Module> {
    return if (Compiler.mode == Compiler.Mode.File) {
        retrieveModulesFileMode()
    } else {
        retrieveModulesDirectoryMode()
    }
}

fun buildModules(modules: List<Module>) {
    Module.dependencySort(modules).let { modules ->
        modules.forEach { module ->
            buildModule(module, modules)
        }
        modules.filterNot(Module::isTransient).forEachParallel { module ->
            module.write()
        }
    }
}

private fun resolveExternalImportTo(modules: MutableList<Module>, import: Import, parents: List<File> = emptyList()) {

    /* Don't process the same module more than once. */
    synchronized(modules) {
        if (modules.any { it.file.path == import.file.path }) {
            return
        }
    }

    /* If this import is a child import of itself, there is a circular dependency. */
    if (parents.any { it.path == import.file.path }) {
        throw MksonCircularExternalModuleDependencyError(parents.last())
    }

    Module(import.file, external = true).let { module ->
        synchronized(modules) {
            modules.add(module)
        }
        parallelStreamOf(module.meta.imports).forEach { subimport ->
            resolveExternalImportTo(modules, subimport, parents + import.file)
        }
    }
}

private fun retrieveModulesFileMode(): List<Module> {
    val main = Module(Compiler.input)
    val modules = mutableListOf(main)
    parallelStreamOf(main.meta.imports).forEach { import ->
        resolveExternalImportTo(modules, import)
    }
    return modules
}

private fun retrieveModulesDirectoryMode(): List<Module> {
    val files = Compiler.input.walk().filter { it.extension == "mkson" && it.isFile }.map { it.canonicalFile }.toList()
    val modules = mutableListOf<Module>()
    parallelStreamOf(files).map { Module(it) }.forEach { module ->
        val external = module.meta.imports.filter { import -> modules.none { it.file.path == import.file.path } }
        synchronized(modules) {
            modules.add(module)
        }
        parallelStreamOf(external).forEach { import ->
            resolveExternalImportTo(modules, import)
        }
    }
    return modules
}
