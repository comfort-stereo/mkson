
import antlrgen.MKSONParser.*
import extensions.isAbsolutePath
import extensions.slash
import extensions.stripQuotes
import extensions.toCanonicalFileOrElse
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.TerminalNode
import java.io.File

private val ALIAS_REGEX = Regex("[_a-zA-Z][_a-zA-Z0-9]*")

data class MetaInformation(val imports: List<Import>, val directives: List<Directive>, val scripts: MutableList<Script>)
data class Import(val file: File, val alias: String)
data class Script(val file: File, val alias: String)
enum class Directive {
    Transient;
    companion object {
        fun parse(string: String) = when (string) {
            "transient" -> Directive.Transient
            else -> null
        }
    }
}

fun getMetaInformation(tree: MksonContext, file: File): MetaInformation {
    val imports = mutableListOf<Import>()
    val directives = mutableListOf<Directive>()
    val scripts = mutableListOf<Script>()

    for (meta in tree.children.asSequence().filterIsInstance<MetaContext>()) {
        val statement = meta.children.single() as ParserRuleContext
        when (statement) {
            is ImportStatementContext -> imports.addAll(parseImportStatement(statement, file))
            is DirectiveStatementContext -> directives.addAll(parseDirectiveStatement(statement, file))
            is ScriptStatementContext -> scripts.addAll(parseScriptStatement(statement, file))
        }
    }

    for (import in imports) {
        imports.find { it.alias == import.alias && it !== import }?.let { conflict ->
            throw MksonConflictingImportAliasError(file, conflict.alias)
        }
    }
    for (script in scripts) {
        scripts.find { it.alias == script.alias && it !== script }?.let { conflict ->
            throw MksonConflictingScriptAliasError(file, conflict.alias)
        }
    }

    return MetaInformation(imports, directives, scripts)
}

private fun parseImportStatement(node: ImportStatementContext, module: File): List<Import> {

    val children = node.children.filter { it !is TerminalNode }

    return mutableListOf<Import>().apply {
        for ((i, child) in children.withIndex()) {
            if (child !is PathContext) continue

            val path = child.text.stripQuotes().let { path ->
                if (path.isAbsolutePath) path else module.parentFile.path slash path
            }
            val file = path.toCanonicalFileOrElse {
                throw MksonImportedModuleDNEError(module, path, module.path)
            }
            val alias = (children.getOrNull(i + 1) as? AliasContext)?.text ?: file.nameWithoutExtension.apply {
                matches(ALIAS_REGEX) || throw MksonInvalidImplicitImportAliasError(file, file.path)
            }
            add(Import(file, alias))
        }
    }
}

private fun parseDirectiveStatement(node: DirectiveStatementContext, module: File): List<Directive> {
    return node.children.filterIsInstance<DirectiveContext>().map { child ->
        Directive.parse(child.text) ?: throw MksonUnrecognizedModuleDirectiveError(module, child.text)
    }
}

private fun parseScriptStatement(node: ScriptStatementContext, module: File): List<Script> {

    val children = node.children.filter { it !is TerminalNode }

    return mutableListOf<Script>().apply {
        for ((i, child) in children.withIndex()) {
            if (child !is PathContext) continue

            val path = child.text.stripQuotes().let { path ->
                if (path.isAbsolutePath) path else module.parentFile.path slash path
            }
            val file = path.toCanonicalFileOrElse {
                throw MksonImportedScriptDNEError(module, path, module.path)
            }
            val alias = (children.getOrNull(i + 1) as? AliasContext)?.text ?: File(path).nameWithoutExtension.apply {
                matches(ALIAS_REGEX) || throw MksonInvalidImplicitScriptAliasError(file, path)
            }
            add(Script(file, alias))
        }
    }
}


