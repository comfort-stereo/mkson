package utilities

data class PositionalArgument(val argument: String, val position: Int)
data class OptionalArgument(val argument: String, val target: String?)

fun getPositionalAndOptionalArguments(arguments: Array<String>): Pair<List<PositionalArgument>, List<OptionalArgument>> {
    val positionalArguments = mutableListOf<PositionalArgument>()
    val optionalArguments = mutableListOf<OptionalArgument>()
    var skip = false
    var position = 0

    for ((i, argument) in arguments.withIndex()) {
        if (skip) {
            skip = false
            continue
        }
        if (argument.startsWith('-')) {
            val optionalArgument = if ('=' in argument) {
                OptionalArgument(argument.substringBefore('='), argument.substringAfter('='))
            } else if (i == arguments.lastIndex || arguments[i + 1].startsWith('-')) {
                OptionalArgument(argument, null)
            } else {
                skip = true
                OptionalArgument(argument, arguments[i + 1])
            }
            optionalArguments.add(optionalArgument)
        } else {
            positionalArguments.add(PositionalArgument(argument, position++))
        }
    }

    return positionalArguments to optionalArguments
}

