package utilities

class CircularDependencyException(val offender: Any) : Exception() {
    override val message = "Circular dependency containing $offender detected."
}

fun <T> Iterable<T>.topologicalSort(depends: (T, T) -> Boolean): List<T> {
    val order = mutableListOf<T>()
    val visited = mutableListOf<T>()
    val seenOnBranch = mutableSetOf<T>()

    for (node in this) {
        seenOnBranch.clear()
        visit(node, this, depends, seenOnBranch, visited, order)
    }
    return order
}

private fun <T> visit(node: T, nodes: Iterable<T>, depends: (T, T) -> Boolean, seenOnBranch: MutableSet<T>,
                      visited: MutableList<T>, order: MutableList<T>) {

    if (!seenOnBranch.add(node)) {
        throw CircularDependencyException(node as Any)
    }

    if (node !in visited) {
        visited.add(node)
        for (other in nodes) if (depends(node, other)) {
            visit(other, nodes, depends, seenOnBranch, visited, order)
        }
        order.add(node)
    }
    seenOnBranch.remove(node)
}

