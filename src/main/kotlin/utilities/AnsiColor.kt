package utilities

object AnsiColor {
    const val Reset = "\u001B[0m"
    const val Black = "\u001B[30m"
    const val Red = "\u001B[31m"
    const val Green = "\u001B[32m"
    const val Yellow = "\u001B[33m"
    const val Blue = "\u001B[34m"
    const val Purple = "\u001B[35m"
    const val Cyan = "\u001B[36m"
    const val White = "\u001B[37m"

    fun color(string: String, color: String) = color + string + Reset
}

