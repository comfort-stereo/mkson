package utilities
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.LinkedBlockingQueue
import java.util.stream.Stream
import java.util.stream.StreamSupport

fun <T> Collection<T>.parallelStream(): Stream<T> {
    return StreamSupport.stream<T>(Spliterators.spliterator(this, 0), true)
}

fun <T> parallelStreamOf(collection: Collection<T>): Stream<T> {
    return StreamSupport.stream<T>(Spliterators.spliterator(collection, 0), true)
}

fun <T> streamOf(collection: Collection<T>): Collection<T> {
    return collection
}

fun <T> Collection<T>.forEachParallel(action: (T) -> Unit) {
    parallelStream().forEach(action)
}

fun <T> concurrentQueueOf(vararg elements: T): ConcurrentLinkedQueue<T> {
    return ConcurrentLinkedQueue<T>().apply { addAll(elements) }
}

fun <T> blockingQueueOf(vararg elements: T): BlockingQueue<T> {
    return LinkedBlockingQueue<T>().apply { addAll(elements) }
}

fun <T> concurrentSetOf(vararg elements: T): ConcurrentSkipListSet<T> {
    return ConcurrentSkipListSet<T>().apply { addAll(elements) }
}

