package utilities

import java.io.File

fun runCommand(command: String, workingDirectory: File): Pair<String, Int> {
    val parts = command.trim().split("\\s".toRegex())
    val proc = ProcessBuilder(*parts.toTypedArray())
        .directory(workingDirectory)
        .redirectOutput(ProcessBuilder.Redirect.PIPE)
        .redirectError(ProcessBuilder.Redirect.PIPE)
        .start()
    val code = proc.waitFor()
    val text = proc.inputStream.bufferedReader().readText()
    return text to code
}
