
import extensions.inode
import extensions.remove
import extensions.replaceSuffix
import extensions.slash
import utilities.CircularDependencyException
import utilities.topologicalSort
import java.io.File

class Module(val file: File, external: Boolean = false) {
    val tree = parseMksonFile(file)
    val meta = getMetaInformation(tree, file)
    val isTransient = Directive.Transient in meta.directives || external
    val outfile = if (isTransient) generateTemporaryFile(file) else generateRelativeOutputFile(file)
    var output: MksonValue? = null

    fun dependsOn(module: Module): Boolean = meta.imports.any { module.matches(it) }
    fun matches(import: Import): Boolean = import.file.path == file.path
    fun generateImportMap(modules: List<Module>): Map<String, Module> {
        return mutableMapOf<String, Module>().apply {
            meta.imports.forEach { import ->
                modules.find { it.matches(import) }?.let { module ->
                    put(import.alias, module)
                }
            }
        }
    }

    fun generateScriptMap(): Map<String, Script> {
        return mutableMapOf<String, Script>().apply {
            for (script in meta.scripts) put(script.alias, script)
        }
    }

    fun finalize(value: MksonValue) {
        tree.children = emptyList()
        output = value
    }

    fun write() {
        val output = output ?: throw MksonShouldNotHappenError()
        val text = output.toJSONString(Compiler.options.indent)

        outfile.parentFile.mkdirs()
        outfile.writeText(text)

        if (Compiler.options.stdout) {
            if (Compiler.mode == Compiler.Mode.Directory) println("<<< ${outfile.name} >>>")
            println(text)
        }
    }

    companion object {
        fun dependencySort(modules: List<Module>): List<Module> {
            return try {
                modules.topologicalSort(Module::dependsOn)
            } catch (exception: CircularDependencyException) {
                throw MksonCircularModuleDependencyError((exception.offender as Module).file)
            }
        }

        private fun generateTemporaryFile(file: File): File {
            return File.createTempFile("${file.nameWithoutExtension}-${file.inode}", ".mkson")
        }

        private fun generateRelativeOutputFile(file: File): File {
            val outputPath = Compiler.output.path
            val relativePath = file.path.remove(Compiler.input.path).replaceSuffix(".mkson", ".json")
            return File(outputPath slash relativePath)
        }
    }
}
