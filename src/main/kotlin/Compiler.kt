
import extensions.hasValidPath
import extensions.isIntString
import extensions.toCanonicalFile
import org.antlr.v4.runtime.ParserRuleContext
import utilities.OptionalArgument
import utilities.PositionalArgument
import utilities.getPositionalAndOptionalArguments
import java.io.File
import kotlin.system.exitProcess

object Compiler {

    data class Options(val indent: Int, val stdout: Boolean)
    data class Processing(var module: Module?, var node: ParserRuleContext?)

    enum class Mode { File, Directory }

    private val VALID_OPTIONAL_ARGUMENTS = setOf("--stdout")
    private val VALID_TARGETED_OPTIONAL_ARGUMENTS = setOf("--indent")
    private val USAGE = """
                    |[ Usage ]:
                    |    To build directory: mkson <input-directory> <output-directory>
                    |    To build file: mkson <input-file> <output-file>
                    """.trimMargin()


    val processing = Processing(null, null)

    lateinit var input: File
        private set
    lateinit var output: File
        private set
    lateinit var options: Options
        private set
    lateinit var mode: Mode
        private set

    fun alertProcessingModule(module: Module) {
        processing.module = module
    }

    fun alertProcessingNode(node: ParserRuleContext) {
        processing.node = node
    }

    fun alertFinishedProcessing() {
        processing.module = null
        processing.node = null
    }

    fun main(argv: Array<String>, testing: Boolean = false) {
        try {
            initialize(argv)
            val modules = retrieveModules()
            buildModules(modules)
        } catch(exception: MksonError) {
            if (testing || exception is MksonShouldNotHappenError) throw exception
            println(exception.msg)
            exitProcess(exception.code)
        }
    }

    private fun initialize(arguments: Array<String>) {
        val (positional, optional) = getPositionalAndOptionalArguments(arguments)
        mode = validateArguments(positional, optional)
        input = positional[0].argument.toCanonicalFile()
        output = positional[1].argument.toCanonicalFile()
        options = Options(
            indent = optional.find { it.argument == "--indent" }?.target?.toInt() ?: 0,
            stdout = optional.any { it.argument == "--stdout" }
        )
    }

    private fun validateArguments(positional: List<PositionalArgument>, optional: List<OptionalArgument>): Mode {
        if (positional.size != 2) throw MksonInvalidUsageError(USAGE)

        val input = File(positional[0].argument)
        val output = File(positional[1].argument)

        if (!input.hasValidPath) throw MksonInputFileHasInvalidPathError(input.path)
        if (!input.exists()) throw MksonInputFileDNEError(input.path)
        if (!output.hasValidPath) throw MksonOutputFileHasInvalidPathError(output.path)

        optional.find { option -> option.argument !in VALID_OPTIONAL_ARGUMENTS &&
                                  option.argument !in VALID_TARGETED_OPTIONAL_ARGUMENTS }?.let { option ->
            throw MksonUnrecognizedOptionalArgumentError(option.argument, USAGE)
        }

        for ((option, target) in optional.filter { it.argument in VALID_TARGETED_OPTIONAL_ARGUMENTS }) {
            when (option) {
                "--indent" -> if (target == null || !target.isIntString() || target.toInt() < 0)
                    throw MksonInvalidOptionalArgumentTarget(option, "must specify a positive integer.")
            }
        }

        return if (input.isFile) { Mode.File } else { Mode.Directory }.also { mode ->
            when (mode) {
                Mode.File -> if (output.isDirectory) throw MksonOutputFileOccupiedByDirectoryError(input.path)
                Mode.Directory -> if (output.isFile) throw MksonOutputDirectoryOccupiedByFileError(input.path)
            }
        }
    }
}
