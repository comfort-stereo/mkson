@file:Suppress("CanBeParameter")

import extensions.trace
import utilities.AnsiColor
import java.io.File

abstract class MksonError(override val message: String = "", open val code: Int = 1) : Exception() {
    abstract val msg: String
    open val exitCode = 1
}

abstract class MksonGenericError : MksonError() {
    override val msg: String get() {
        return AnsiColor.color("Error -> $message", AnsiColor.Red)
    }
}

abstract class MksonInputError : MksonGenericError()

abstract class MksonPrecompilationError(val module: File) : MksonError() {
    override val msg: String get() {
        val moduleName = module.name
        val moduleStatement = if (moduleName.isNotEmpty()) "in '$moduleName' " else ""
        return AnsiColor.color("Error $moduleStatement-> $message", AnsiColor.Red)
    }

}

abstract class MksonCompilationError : MksonError() {
    override val msg: String get() {
        val moduleName = Compiler.processing.module?.file?.name ?: ""
        val trace = Compiler.processing.node?.trace()
        val moduleStatement = if (moduleName.isNotEmpty()) "in '$moduleName' " else ""
        val traceStatement = if (trace != null) "Keytrace: $trace" else ""
        return AnsiColor.color("Error $moduleStatement-> $message \n$traceStatement", AnsiColor.Red)
    }
}

/*
 * Generic Errors
 */

class MksonShouldNotHappenError(override val message: String = "") : MksonGenericError()

/*
 * Input Errors
 */

class MksonInputFileHasInvalidPathError(val file: String) : MksonInputError() {
    override val message = "Input file or directory $file has an invalid path."
}

class MksonInputFileDNEError(val file: String) : MksonInputError() {
    override val message = "Input file or directory $file does not exist"
}

class MksonOutputFileHasInvalidPathError(val file: String) : MksonInputError() {
    override val message = "Output file or directory $file has an invalid path."
}

class MksonOutputDirectoryOccupiedByFileError(val file: String) : MksonInputError() {
    override val message = "Output directory $file is already occupied by a file."
}

class MksonOutputFileOccupiedByDirectoryError(val file: String) : MksonInputError() {
    override val message = "Output directory $file is already occupied by a directory."
}

class MksonInvalidUsageError(val usage: String) : MksonInputError() {
    override val message = usage
}

class MksonUnrecognizedOptionalArgumentError(val argument: String, val usage: String) : MksonInputError() {
    override val message = "Unrecognized optional argument '$argument'. \n $usage"
}

/*
 * Precompilation Errors 
 */

class MksonUnrecognizedModuleDirectiveError(module: File, val directive: String) : MksonPrecompilationError(module) {
    override val message = "Unrecognized module directive '$directive'."
}

class MksonCircularModuleDependencyError(module: File): MksonPrecompilationError(module) {
    override val message = "Circular dependency detected."
}

class MksonCircularExternalModuleDependencyError(module: File) : MksonPrecompilationError(module) {
    override val message = "Circular dependency involving external module $module."
}

class MksonImportedModuleDNEError(module: File, val importedModule: String, val externalModule: String) : MksonPrecompilationError(module) {
    override val message = "Module $importedModule imported by module $externalModule does not exist."
}

class MksonImportedScriptDNEError(module: File, val path: String, val externalModule: String) : MksonPrecompilationError(module) {
    override val message = "Script $path imported by module $externalModule does not exist."
}

class MksonConflictingImportAliasError(module: File, val alias: String) : MksonPrecompilationError(module) {
    override val message = "Conflicting candidates for import alias '$alias'."
}

class MksonConflictingScriptAliasError(module: File, val alias: String) : MksonPrecompilationError(module) {
    override val message = "Conflicting candidates for script alias '$alias'."
}

class MksonInvalidImplicitImportAliasError(module: File, val importedModule: String) : MksonPrecompilationError(module) {
    override val message = "Imported module $importedModule has a file name containing invalid characters for an " +
        "implicit alias. Alias the import with an 'as' statement."
}

class MksonInvalidImplicitScriptAliasError(module: File, val path: String) : MksonPrecompilationError(module) {
    override val message = "Imported script $path has a file name containing invalid characters for an " +
            "implicit alias. Alias the script with an 'as' statement."
}

class MksonSyntaxError(module: File, val line: Int, val column: Int, val symbol: String, val section: String) : MksonPrecompilationError(module) {
    override val message = "Syntax error near line, column ($line, $column) : $symbol \n$section"
}

/*
 * Compilation Errors
 */

class MksonFailedParseError: MksonCompilationError()

class MksonInvalidPathError(val offendingToken: String) : MksonCompilationError() {
    override val message: String = "Invalid path starting at '$offendingToken'."
}

class MksonCircularReferenceError(val reference: String) : MksonCompilationError() {
    override val message = "Circular dependency involving reference '$reference'."
}

class MksonInvalidBinaryOperationError(val operator: String, val left: String, val right: String) : MksonCompilationError() {
    override val message = "Operator '$operator' could not be applied to $left and $right."
}

class MksonInvalidUnaryOperationError(val operator: String, val operand: String) : MksonCompilationError() {
    override val message = "Operator '$operator' could not be applied to $operand."
}

class MksonReferenceDNEError(val reference: String) : MksonCompilationError() {
    override val message = "Value specified by reference '$reference' does not exist."
}

class MksonSelfReferenceDNEError(val reference: String) : MksonCompilationError() {
    override val message = "Value specified by self reference '$reference' does not exist."
}

class MksonInheritanceFromNonObjectValueError(val value: String) : MksonCompilationError() {
    override val message = "Inheritance from non-object value specified by value '$value'."
}

class MksonTopNameConflictError(val alias: String) : MksonCompilationError() {
    override val message = "Alias of import '$alias' conflicts with module key of the same name."
}

class MksonInvalidOptionalArgumentTarget(val option: String, val help: String) : MksonCompilationError() {
    override val message = "Optional argument $option $help"
}

class MksonKeyDoesNotExistError(val key: String) : MksonCompilationError() {
    override val message = "Key '$key' does not exist on specified object."
}

class MksonIndexOutOfBoundsError(val index: Int) : MksonCompilationError() {
    override val message = "Index $index is out of bounds."
}

class MksonDivisionByZeroError : MksonCompilationError() {
    override val message = "Attempted to divide by zero."
}

class MksonScriptDNEError(val namespace: String) : MksonCompilationError() {
    override val message = "Script '$namespace' referenced in function call is not defined."
}

class MksonUnresolvedBuiltInFunctionError(val function: String, val arguments: List<MksonValue>) : MksonCompilationError() {
    override val message = "Function $function(${arguments.joinToString(transform = MksonValue::typeName)}) is not a built-in-function."
}

class MksonUnresolvedCustomFunctionError(val namespace: String, val function: String, val arguments: List<MksonValue>) : MksonCompilationError() {
    override val message = "Function $function(${arguments.joinToString(transform = MksonValue::typeName)}) is was not defined in script '$namespace'."
}

class MksonScriptingNotAvailableError : MksonCompilationError() {
    override val message = "Custom function scripting is not available on your current platform."
}

class MksonScriptNotParsableError(val script: Script) : MksonCompilationError() {
    override val message = "Script '${script.alias}' at ${script.file.path} was not parsable."
}

class MksonInvalidFunctionArgumentsError(val signature: String, val reason: String) : MksonCompilationError() {
    override val message = "Invalid arguments to function $signature: $reason"
}

class MksonAttemptToAccessArrayAncestorError : MksonCompilationError() {
    override val message = "Attempted to access ancestor of array."
}

