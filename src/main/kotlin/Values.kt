
import antlrgen.MKSONParser.ValueContext
import extensions.addOrRemove
import extensions.escapeSpecialCharacters
import extensions.unescapeSpecialCharacters
import jdk.nashorn.api.scripting.JSObject
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

val MKSON_OBJECT = 0
val MKSON_ARRAY = 1
val MKSON_STRING = 2
val MKSON_NUMBER = 3
val MKSON_BOOLEAN = 4
val MKSON_NULL = 5
val MKSON_UNDEFINED = 6

val MKSON_OBJECT_TYPENAME = "object"
val MKSON_ARRAY_TYPENAME = "array"
val MKSON_STRING_TYPENAME = "string"
val MKSON_NUMBER_TYPENAME = "number"
val MKSON_BOOLEAN_TYPENAME = "boolean"
val MKSON_NULL_TYPENAME = "null"
val MKSON_UNDEFINED_TYPENAME = "undefined"

fun typeToTypeName(type: Int): String {
    return when (type) {
        MKSON_OBJECT -> MKSON_OBJECT_TYPENAME
        MKSON_ARRAY -> MKSON_ARRAY_TYPENAME
        MKSON_STRING -> MKSON_STRING_TYPENAME
        MKSON_NUMBER -> MKSON_NUMBER_TYPENAME
        MKSON_BOOLEAN -> MKSON_BOOLEAN_TYPENAME
        MKSON_NULL -> MKSON_NULL_TYPENAME
        MKSON_UNDEFINED -> MKSON_UNDEFINED_TYPENAME
        else -> throw MksonShouldNotHappenError()
    }
}

interface MksonValue {
    var isReferenced: Boolean
    val type: Int
    val typeName: String
    val value: Any?

    fun isTruthy(): Boolean
    fun copy(): MksonValue
    fun has(value: MksonValue): Boolean = throw UnsupportedOperationException()
    fun operation(operator: String, operand: MksonValue): MksonValue = throw UnsupportedOperationException()
    fun operation(operator: String): MksonValue = generalUnaryOperation(operator) ?: throw UnsupportedOperationException()

    fun generalOperation(operator: String, operand: MksonValue): MksonValue? {
        return when (operator) {
            "in" -> MksonBoolean(operand.has(this))
            "!in" -> MksonBoolean(!operand.has(this))
            "==" -> MksonBoolean(this == operand)
            "!=" -> MksonBoolean(this != operand)
            else -> return null
        }
    }

    fun generalUnaryOperation(operator: String): MksonValue? {
        return when (operator) {
            "!" -> MksonBoolean(!isTruthy())
            else -> return null
        }
    }

    override fun toString(): String
    override fun equals(other: Any?): Boolean
    fun toJSONString(): String
    fun toPrettyJSONString(indent: Int, currentIndent: Int = 0): String = toJSONString()

    fun operate(operator: String, operand: MksonValue): MksonValue? {
        try {
            return operation(operator, operand)
        } catch (exception: ClassCastException) {
        } catch (exception: UnsupportedOperationException) {
        }
        return null
    }

    fun operate(operator: String): MksonValue? {
        try {
            return operation(operator)
        } catch (exception: ClassCastException) {
        } catch (exception: UnsupportedOperationException) {
        }
        return null
    }

    fun toJSONString(indent: Int): String {
        removeTransientKeys()
        return if (indent == 0) toJSONString() else toPrettyJSONString(indent, 0)
    }

    fun removeTransientKeys() {
        if (type == MKSON_OBJECT) {
            this as MksonObject
            for (key in transientKeys) remove(key)
            for (key in keys) get(key)?.removeTransientKeys()
        } else if (type == MKSON_ARRAY) {
            for (value in this as MksonArray) value.removeTransientKeys()
        }
    }

    fun toJSON(): Any? {
        return when (this) {
            is MksonObject -> mutableMapOf<String, Any?>().also { obj ->
                for ((key, value) in entries) obj.put(key, value.toJSON())
            }
            is MksonArray -> mutableListOf<Any?>().also { array ->
                for (value in this) array.add(value.toJSON())
            }
            is MksonString -> value.toString()
            else -> value
        }
    }

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun fromGeneralValue(value: Any?): MksonValue {
            
            if (value is JSObject && value.isArray) return fromGeneralValue(value.values())

            return when (value) {
                is String -> MksonString(value)
                is Int -> MksonNumber(value)
                is Double -> MksonNumber(value)
                is Boolean -> MksonBoolean(value)
                is Map<*, *> -> MksonObject(value as Map<String, Any?>)
                is List<*> -> MksonArray(value)
                is JSONObject -> MksonObject(value)
                is JSONArray -> MksonArray(value)
                null -> MksonNull
                else -> throw MksonFailedParseError()
            }
        }

        fun fromJSONString(string: String): MksonValue? {
            return try {
                fromGeneralValue(JSONArray("[$string]")[0])
            } catch (exception: MksonFailedParseError) {
                null
            }
        }
    }
}

class MksonObject : LinkedHashMap<String, MksonValue>, MksonValue {
    override var isReferenced: Boolean = false
    val transientKeys = mutableSetOf<String>()
    val recompilationMap = mutableMapOf<String, ValueContext>()
    var ancestor: MksonObject? = null
        private set

    override val type: Int get() = MKSON_OBJECT
    override val typeName: String get() = MKSON_OBJECT_TYPENAME
    override val value: MksonObject get() = this

    constructor() : super()
    constructor(obj: MksonObject) : super(obj) {
        transientKeys.addAll(obj.transientKeys)
        recompilationMap.putAll(obj.recompilationMap)
        ancestor = obj.ancestor
    }

    constructor(obj: Map<String, Any?>) : super(obj.size) {
        for ((key, value) in obj) this[key] = MksonValue.fromGeneralValue(value)
    }

    constructor(obj: JSONObject) : super(obj.length()) {
        for (key in obj.keys()) this[key] = MksonValue.fromGeneralValue(obj[key])
    }

    override fun isTruthy(): Boolean = isNotEmpty()
    override fun copy(): MksonObject = MksonObject(this)
    fun applyCopy(block: MksonObject.() -> Unit): MksonObject = if (isReferenced) { copy() } else { this }.apply(block)

    override fun has(value: MksonValue): Boolean {
        return (value as MksonString).value.toString() in this.keys
    }

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        generalOperation(operator, operand)?.let { return it }
        return when (operator) {
            "[" -> {
                val key = (operand as MksonString).value.toString()
                this[key]?.apply { isReferenced = true } ?: throw MksonKeyDoesNotExistError(key)
            }
            "+" -> applyCopy { merge(operand as MksonObject) }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun toString(): String = toJSONString()

    override fun toJSONString(): String {
        if (isEmpty()) return "{}"
        return buildString {
            append('{')
            for ((key, value) in entries) {
                append("\"", key, "\":", value.toJSONString(), ",")
            }
            set(lastIndex, '}')
        }
    }

    override fun toPrettyJSONString(indent: Int, currentIndent: Int): String {
        if (isEmpty()) return "{}"
        return buildString {
            append("{\n")
            for ((key, value) in entries) {
                val string = value.toPrettyJSONString(indent, currentIndent + indent)
                append(" ".repeat(currentIndent + indent), "\"", key, "\": ", string, ",\n")
            }
            deleteCharAt(lastIndex - 1)
            append(" ".repeat(currentIndent), "}")
        }
    }

    fun getAncestor(ancestor: Int) : MksonObject? {
        var current: MksonObject? = this
        var count = 0
        while (count++ < ancestor) {
            current = current?.ancestor ?: return null
        }
        return current
    }

    fun get(key: String, ancestor: Int): MksonValue? {
        return getAncestor(ancestor)?.get(key)
    }

    fun inherit(other: MksonObject) {
        putAll(other)
        transientKeys.addAll(other.transientKeys)
        ancestor = other
    }

    fun merge(other: MksonObject) {
        putAll(other)
        for (key in other.keys) {
            transientKeys.addOrRemove(key, add = key in other.transientKeys)
            if (key in other.recompilationMap) {
                recompilationMap[key] = other.recompilationMap.getValue(key)
            } else {
                recompilationMap.remove(key)
            }
        }
    }
}

class MksonArray : ArrayList<MksonValue>, MksonValue {
    override var isReferenced: Boolean = false

    override val type: Int get() = MKSON_ARRAY
    override val typeName: String get() = MKSON_ARRAY_TYPENAME
    override val value: MksonArray get() = this

    constructor() : super()
    constructor(size: Int) : super(size)
    constructor(array: MksonArray) : super(array)
    constructor(array: List<Any?>) : super(array.size) {
        for (element in array) add(MksonValue.fromGeneralValue(element))
    }

    constructor(array: JSONArray) : super(array.length()) {
        for (element in array) add(MksonValue.fromGeneralValue(element))
    }

    override fun isTruthy(): Boolean = isNotEmpty()
    override fun copy(): MksonArray = MksonArray(this)
    fun applyCopy(block: MksonArray.() -> Unit): MksonArray = if (isReferenced) { copy() } else { this }.apply(block)
    override fun has(value: MksonValue): Boolean = contains(value)

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        generalOperation(operator, operand)?.let { return it }
        val result = when (operator) {
            "[" -> {
                val index = (operand as MksonNumber).value.toInt()
                getOrNull(index)?.apply { isReferenced = true } ?: throw MksonIndexOutOfBoundsError(index)
            }
            "+" -> applyCopy { addAll(operand as MksonArray) }
            else -> throw UnsupportedOperationException()
        }
        return result
    }

    override fun toString(): String = toJSONString()

    override fun toJSONString(): String {
        if (isEmpty()) return "[]"
        val array = this
        return buildString {
            append("[")
            for (value in array) append(value.toJSONString(), ",")
            set(lastIndex, ']')
        }
    }

    override fun toPrettyJSONString(indent: Int, currentIndent: Int): String {
        if (isEmpty()) return "[]"
        return buildString {
            append("[\n")
            for (value in this@MksonArray) {
                val string = value.toPrettyJSONString(indent, currentIndent + indent)
                append(" ".repeat(currentIndent + indent), string, ",\n")
            }
            deleteCharAt(lastIndex - 1)
            append(" ".repeat(currentIndent), "]")
        }
    }
}

class MksonString : MksonValue {
    override var isReferenced: Boolean = false
    private var inner: StringBuilder = StringBuilder()
    
    override val type: Int get() = MKSON_STRING
    override val typeName: String get() = MKSON_STRING_TYPENAME
    override val value: StringBuilder get() = inner

    constructor()
    constructor(size: Int) {
        inner = StringBuilder(size)
    }

    constructor(string: String) {
        inner = StringBuilder(string).apply {
            unescapeSpecialCharacters()
        }
    }

    constructor(stringBuilder: StringBuilder) {
        inner = stringBuilder.apply {
            unescapeSpecialCharacters()
        }
    }

    override fun isTruthy(): Boolean = inner.isNotEmpty()
    override fun copy(): MksonString = MksonString(StringBuilder(inner))
    fun applyCopy(block: MksonString.() -> Unit): MksonString = if (isReferenced) { copy() } else { this }.apply(block)
    override fun has(value: MksonValue): Boolean = (value as MksonString).inner in inner
    override fun equals(other: Any?): Boolean = other is MksonString && inner.toString() == other.inner.toString()
    override fun hashCode(): Int = inner.hashCode()

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        generalOperation(operator, operand)?.let { return it }
        if (operand.type == MKSON_NUMBER) when (operator) {
            "[" -> {
                val index = (operand as MksonNumber).value.toInt()
                return MksonString(inner.getOrNull(index)?.toString() ?: throw MksonIndexOutOfBoundsError(index))
            }
        }
        operand as MksonString
        return when (operator) {
            "+" -> applyCopy { inner.append((operand).inner) }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun toString(): String = inner.toString()

    override fun toJSONString(): String {
        return StringBuilder(inner).apply {
            escapeSpecialCharacters()
            insert(0, '\"')
            append('\"')
        }.toString()
    }
}

class MksonNumber(number: Number) : MksonValue {
    override var isReferenced: Boolean = false
    private var inner = number.toDouble()

    override val type: Int get() = MKSON_NUMBER
    override val typeName: String get() = MKSON_NUMBER_TYPENAME
    override var value: Double
        get() = inner
        set(value) {
            inner = value
        }

    override fun isTruthy(): Boolean = inner != 0.0
    override fun copy(): MksonNumber = MksonNumber(inner)
    fun applyCopy(block: MksonNumber.() -> Unit): MksonNumber = if (isReferenced) { copy() } else { this }.apply(block)
    override fun equals(other: Any?): Boolean = other is MksonNumber && inner == other.inner
    override fun hashCode(): Int = inner.hashCode()

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        generalOperation(operator, operand)?.let { return it }
        when (operand.type) { MKSON_STRING, MKSON_ARRAY -> return operand.operation(operator, this) }
        operand as MksonNumber
        when (operator) { "/", "\\", "%" -> if (operand.inner == 0.0) throw MksonDivisionByZeroError() }
        val result = when (operator) {
            "+" -> applyCopy { inner += operand.inner }
            "-" -> applyCopy { inner -= operand.inner }
            "*" -> applyCopy { inner *= operand.inner }
            "/" -> applyCopy { inner /= operand.inner }
            "%" -> applyCopy { inner %= operand.inner }
            "\\" -> applyCopy { inner = Math.floor(inner / operand.inner) }
            "^" -> applyCopy { inner = Math.pow(inner, operand.inner) }
            ">=" -> MksonBoolean(inner >= operand.inner)
            "<=" -> MksonBoolean(inner <= operand.inner)
            ">" -> MksonBoolean(inner > operand.inner)
            "<" -> MksonBoolean(inner < operand.inner)
            else -> throw UnsupportedOperationException()
        }

        return result
    }

    override fun operation(operator: String): MksonValue {
        generalUnaryOperation(operator)?.let { return it }
        return when (operator) {
            "-" -> applyCopy { inner = -inner }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun toString(): String = inner.toString().removeSuffix(".0")
    override fun toJSONString(): String = toString()
}

class MksonBoolean(boolean: Boolean) : MksonValue {
    override var isReferenced: Boolean = false
    private var inner = boolean

    override val type: Int get() = MKSON_BOOLEAN
    override val typeName: String get() = MKSON_BOOLEAN_TYPENAME
    override val value: Boolean get() = inner

    override fun isTruthy(): Boolean = inner
    override fun copy(): MksonBoolean = MksonBoolean(inner)
    fun applyCopy(block: MksonBoolean.() -> Unit): MksonBoolean = if (isReferenced) { copy() } else { this }.apply(block)
    override fun equals(other: Any?): Boolean = other is MksonBoolean && inner == other.inner
    override fun hashCode(): Int = inner.hashCode()

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        generalOperation(operator, operand)?.let { return it }
        operand as MksonBoolean
        return when (operator) {
            "&&" -> applyCopy { inner = inner && operand.inner }
            "||" -> applyCopy { inner = inner || operand.inner }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun toString(): String = inner.toString()
    override fun toJSONString(): String = toString()
}

object MksonNull : MksonValue {
    override var isReferenced: Boolean
        get() = false
        set(value) {}
    override val type: Int get() = MKSON_NULL
    override val typeName: String get() = MKSON_NULL_TYPENAME
    override val value: Any? get() = null

    override fun isTruthy(): Boolean = false
    override fun copy(): MksonNull = this
    override fun equals(other: Any?): Boolean = other === this
    override fun hashCode(): Int = typeName.hashCode()

    override fun operation(operator: String, operand: MksonValue): MksonValue {
        return generalOperation(operator, operand) ?: throw UnsupportedOperationException()
    }

    override fun toString(): String = "null"
    override fun toJSONString(): String = toString()
}

object MksonUndefined: MksonValue {
    override var isReferenced: Boolean
        get() = false
        set(value) {}
    override val type: Int get() = MKSON_UNDEFINED
    override val typeName: String get() = MKSON_UNDEFINED_TYPENAME
    override val value: Any? get() = null

    override fun isTruthy(): Boolean = false
    override fun copy(): MksonUndefined = this
    override fun equals(other: Any?): Boolean = other === this
    override fun hashCode(): Int = typeName.hashCode()
    
    override fun operation(operator: String): MksonValue = this
    override fun operation(operator: String, operand: MksonValue): MksonValue = this

    override fun toString(): String = "???"
    override fun toJSONString(): String = "\"${toString()}\""
}
