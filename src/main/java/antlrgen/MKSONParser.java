// Generated from MKSONParser.g4 by ANTLR 4.6

package antlrgen;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MKSONParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		KeywordImport=1, KeywordModule=2, KeywordScript=3, KeywordAs=4, KeywordIf=5, 
		KeywordIn=6, KeywordNotIn=7, KeywordElse=8, KeywordTrue=9, KeywordFalse=10, 
		KeywordNull=11, Comment=12, LineComment=13, Reference=14, ReferenceIndicator=15, 
		ModuleReferenceIndicator=16, SelfReferenceIndicator=17, ID=18, String=19, 
		Number=20, Dot=21, Asterisk=22, OpenBrace=23, ClosedBrace=24, OpenBracket=25, 
		ClosedBracket=26, OpenParen=27, ClosedParen=28, ExclamationMark=29, Dash=30, 
		Caret=31, Slash=32, Backslash=33, Modulus=34, Plus=35, Equals=36, NotEquals=37, 
		GreaterThan=38, LessThan=39, GreaterThanOrEqual=40, LessThanOrEqual=41, 
		And=42, Or=43, Comma=44, Assignment=45, Colon=46, DoubleColon=47, Quote=48, 
		QuestionMark=49, WS=50;
	public static final int
		RULE_mkson = 0, RULE_meta = 1, RULE_importStatement = 2, RULE_directiveStatement = 3, 
		RULE_scriptStatement = 4, RULE_path = 5, RULE_alias = 6, RULE_directive = 7, 
		RULE_value = 8, RULE_ifStatement = 9, RULE_reference = 10, RULE_functionCall = 11, 
		RULE_inheritance = 12, RULE_object = 13, RULE_pair = 14, RULE_array = 15, 
		RULE_string = 16, RULE_number = 17, RULE_bool = 18, RULE_nulltype = 19, 
		RULE_undefined = 20;
	public static final String[] ruleNames = {
		"mkson", "meta", "importStatement", "directiveStatement", "scriptStatement", 
		"path", "alias", "directive", "value", "ifStatement", "reference", "functionCall", 
		"inheritance", "object", "pair", "array", "string", "number", "bool", 
		"nulltype", "undefined"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'import'", "'module'", "'script'", "'as'", "'if'", "'in'", "'!in'", 
		"'else'", "'true'", "'false'", "'null'", null, null, null, null, "'@'", 
		null, null, null, null, "'.'", "'*'", "'{'", "'}'", "'['", "']'", "'('", 
		"')'", "'!'", "'-'", "'^'", "'/'", "'\\'", "'%'", "'+'", "'=='", "'!='", 
		"'>'", "'<'", "'>='", "'<='", "'&&'", "'||'", "','", "'='", "':'", "'::'", 
		"'\"'", "'?'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "KeywordImport", "KeywordModule", "KeywordScript", "KeywordAs", 
		"KeywordIf", "KeywordIn", "KeywordNotIn", "KeywordElse", "KeywordTrue", 
		"KeywordFalse", "KeywordNull", "Comment", "LineComment", "Reference", 
		"ReferenceIndicator", "ModuleReferenceIndicator", "SelfReferenceIndicator", 
		"ID", "String", "Number", "Dot", "Asterisk", "OpenBrace", "ClosedBrace", 
		"OpenBracket", "ClosedBracket", "OpenParen", "ClosedParen", "ExclamationMark", 
		"Dash", "Caret", "Slash", "Backslash", "Modulus", "Plus", "Equals", "NotEquals", 
		"GreaterThan", "LessThan", "GreaterThanOrEqual", "LessThanOrEqual", "And", 
		"Or", "Comma", "Assignment", "Colon", "DoubleColon", "Quote", "QuestionMark", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MKSONParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MKSONParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MksonContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public List<MetaContext> meta() {
			return getRuleContexts(MetaContext.class);
		}
		public MetaContext meta(int i) {
			return getRuleContext(MetaContext.class,i);
		}
		public MksonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mkson; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterMkson(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitMkson(this);
		}
	}

	public final MksonContext mkson() throws RecognitionException {
		MksonContext _localctx = new MksonContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_mkson);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KeywordImport) | (1L << KeywordModule) | (1L << KeywordScript))) != 0)) {
				{
				{
				setState(42);
				meta();
				}
				}
				setState(47);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(48);
			value(0);
			setState(52);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KeywordImport) | (1L << KeywordModule) | (1L << KeywordScript))) != 0)) {
				{
				{
				setState(49);
				meta();
				}
				}
				setState(54);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetaContext extends ParserRuleContext {
		public ImportStatementContext importStatement() {
			return getRuleContext(ImportStatementContext.class,0);
		}
		public DirectiveStatementContext directiveStatement() {
			return getRuleContext(DirectiveStatementContext.class,0);
		}
		public ScriptStatementContext scriptStatement() {
			return getRuleContext(ScriptStatementContext.class,0);
		}
		public MetaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meta; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterMeta(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitMeta(this);
		}
	}

	public final MetaContext meta() throws RecognitionException {
		MetaContext _localctx = new MetaContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_meta);
		try {
			setState(58);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KeywordImport:
				enterOuterAlt(_localctx, 1);
				{
				setState(55);
				importStatement();
				}
				break;
			case KeywordModule:
				enterOuterAlt(_localctx, 2);
				{
				setState(56);
				directiveStatement();
				}
				break;
			case KeywordScript:
				enterOuterAlt(_localctx, 3);
				{
				setState(57);
				scriptStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportStatementContext extends ParserRuleContext {
		public List<PathContext> path() {
			return getRuleContexts(PathContext.class);
		}
		public PathContext path(int i) {
			return getRuleContext(PathContext.class,i);
		}
		public List<AliasContext> alias() {
			return getRuleContexts(AliasContext.class);
		}
		public AliasContext alias(int i) {
			return getRuleContext(AliasContext.class,i);
		}
		public ImportStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterImportStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitImportStatement(this);
		}
	}

	public final ImportStatementContext importStatement() throws RecognitionException {
		ImportStatementContext _localctx = new ImportStatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_importStatement);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(KeywordImport);
			setState(61);
			match(OpenBrace);
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==String) {
				{
				setState(71);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(62);
						path();
						setState(65);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==KeywordAs) {
							{
							setState(63);
							match(KeywordAs);
							setState(64);
							alias();
							}
						}

						setState(67);
						match(Comma);
						}
						} 
					}
					setState(73);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				}
				{
				setState(74);
				path();
				setState(77);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==KeywordAs) {
					{
					setState(75);
					match(KeywordAs);
					setState(76);
					alias();
					}
				}

				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(79);
					match(Comma);
					}
				}

				}
			}

			setState(84);
			match(ClosedBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectiveStatementContext extends ParserRuleContext {
		public List<DirectiveContext> directive() {
			return getRuleContexts(DirectiveContext.class);
		}
		public DirectiveContext directive(int i) {
			return getRuleContext(DirectiveContext.class,i);
		}
		public DirectiveStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directiveStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterDirectiveStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitDirectiveStatement(this);
		}
	}

	public final DirectiveStatementContext directiveStatement() throws RecognitionException {
		DirectiveStatementContext _localctx = new DirectiveStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_directiveStatement);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(KeywordModule);
			setState(87);
			match(OpenBrace);
			setState(100);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(93);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(88);
						directive();
						setState(89);
						match(Comma);
						}
						} 
					}
					setState(95);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				}
				setState(96);
				directive();
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(97);
					match(Comma);
					}
				}

				}
			}

			setState(102);
			match(ClosedBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScriptStatementContext extends ParserRuleContext {
		public List<PathContext> path() {
			return getRuleContexts(PathContext.class);
		}
		public PathContext path(int i) {
			return getRuleContext(PathContext.class,i);
		}
		public List<AliasContext> alias() {
			return getRuleContexts(AliasContext.class);
		}
		public AliasContext alias(int i) {
			return getRuleContext(AliasContext.class,i);
		}
		public ScriptStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scriptStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterScriptStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitScriptStatement(this);
		}
	}

	public final ScriptStatementContext scriptStatement() throws RecognitionException {
		ScriptStatementContext _localctx = new ScriptStatementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_scriptStatement);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(KeywordScript);
			setState(105);
			match(OpenBrace);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==String) {
				{
				setState(115);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(106);
						path();
						setState(109);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==KeywordAs) {
							{
							setState(107);
							match(KeywordAs);
							setState(108);
							alias();
							}
						}

						setState(111);
						match(Comma);
						}
						} 
					}
					setState(117);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				}
				{
				setState(118);
				path();
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==KeywordAs) {
					{
					setState(119);
					match(KeywordAs);
					setState(120);
					alias();
					}
				}

				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(123);
					match(Comma);
					}
				}

				}
			}

			setState(128);
			match(ClosedBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(MKSONParser.String, 0); }
		public PathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitPath(this);
		}
	}

	public final PathContext path() throws RecognitionException {
		PathContext _localctx = new PathContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_path);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			match(String);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AliasContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MKSONParser.ID, 0); }
		public AliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitAlias(this);
		}
	}

	public final AliasContext alias() throws RecognitionException {
		AliasContext _localctx = new AliasContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_alias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectiveContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MKSONParser.ID, 0); }
		public DirectiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directive; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterDirective(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitDirective(this);
		}
	}

	public final DirectiveContext directive() throws RecognitionException {
		DirectiveContext _localctx = new DirectiveContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_directive);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public NulltypeContext nulltype() {
			return getRuleContext(NulltypeContext.class,0);
		}
		public UndefinedContext undefined() {
			return getRuleContext(UndefinedContext.class,0);
		}
		public ObjectContext object() {
			return getRuleContext(ObjectContext.class,0);
		}
		public InheritanceContext inheritance() {
			return getRuleContext(InheritanceContext.class,0);
		}
		public ArrayContext array() {
			return getRuleContext(ArrayContext.class,0);
		}
		public ReferenceContext reference() {
			return getRuleContext(ReferenceContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public TerminalNode ID() { return getToken(MKSONParser.ID, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitValue(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		return value(0);
	}

	private ValueContext value(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ValueContext _localctx = new ValueContext(_ctx, _parentState);
		ValueContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_value, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(137);
				string();
				}
				break;
			case 2:
				{
				setState(138);
				number();
				}
				break;
			case 3:
				{
				setState(139);
				bool();
				}
				break;
			case 4:
				{
				setState(140);
				nulltype();
				}
				break;
			case 5:
				{
				setState(141);
				undefined();
				}
				break;
			case 6:
				{
				setState(142);
				object();
				}
				break;
			case 7:
				{
				setState(143);
				inheritance();
				setState(144);
				match(DoubleColon);
				setState(145);
				object();
				}
				break;
			case 8:
				{
				setState(147);
				array();
				}
				break;
			case 9:
				{
				setState(148);
				reference();
				}
				break;
			case 10:
				{
				setState(149);
				functionCall();
				}
				break;
			case 11:
				{
				setState(150);
				match(OpenParen);
				setState(151);
				value(0);
				setState(152);
				match(ClosedParen);
				}
				break;
			case 12:
				{
				setState(154);
				ifStatement();
				}
				break;
			case 13:
				{
				setState(155);
				match(ExclamationMark);
				setState(156);
				value(10);
				}
				break;
			case 14:
				{
				setState(157);
				match(Dash);
				setState(158);
				value(9);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(198);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(196);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(161);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(162);
						match(Caret);
						setState(163);
						value(9);
						}
						break;
					case 2:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(164);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(165);
						match(Asterisk);
						setState(166);
						value(8);
						}
						break;
					case 3:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(167);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(168);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Slash) | (1L << Backslash) | (1L << Modulus))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(169);
						value(7);
						}
						break;
					case 4:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(170);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(171);
						_la = _input.LA(1);
						if ( !(_la==Dash || _la==Plus) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(172);
						value(6);
						}
						break;
					case 5:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(173);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(174);
						_la = _input.LA(1);
						if ( !(_la==KeywordIn || _la==KeywordNotIn) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(175);
						value(5);
						}
						break;
					case 6:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(176);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(177);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Equals) | (1L << NotEquals) | (1L << GreaterThan) | (1L << LessThan) | (1L << GreaterThanOrEqual) | (1L << LessThanOrEqual))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(178);
						value(4);
						}
						break;
					case 7:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(179);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(180);
						match(And);
						setState(181);
						value(3);
						}
						break;
					case 8:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(182);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(183);
						match(Or);
						setState(184);
						value(2);
						}
						break;
					case 9:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(185);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(186);
						match(Dot);
						setState(187);
						functionCall();
						}
						break;
					case 10:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(188);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(189);
						match(Dot);
						setState(190);
						match(ID);
						}
						break;
					case 11:
						{
						_localctx = new ValueContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_value);
						setState(191);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(192);
						match(OpenBracket);
						setState(193);
						value(0);
						setState(194);
						match(ClosedBracket);
						}
						break;
					}
					} 
				}
				setState(200);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitIfStatement(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_ifStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			match(KeywordIf);
			setState(212); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(202);
				match(OpenParen);
				setState(203);
				value(0);
				setState(204);
				match(ClosedParen);
				setState(210);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
				case 1:
					{
					setState(205);
					match(OpenBrace);
					setState(206);
					value(0);
					setState(207);
					match(ClosedBrace);
					}
					break;
				case 2:
					{
					setState(209);
					value(0);
					}
					break;
				}
				}
				}
				setState(214); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==OpenParen );
			setState(216);
			match(KeywordElse);
			setState(222);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(217);
				match(OpenBrace);
				setState(218);
				value(0);
				setState(219);
				match(ClosedBrace);
				}
				break;
			case 2:
				{
				setState(221);
				value(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReferenceContext extends ParserRuleContext {
		public TerminalNode Reference() { return getToken(MKSONParser.Reference, 0); }
		public TerminalNode ReferenceIndicator() { return getToken(MKSONParser.ReferenceIndicator, 0); }
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitReference(this);
		}
	}

	public final ReferenceContext reference() throws RecognitionException {
		ReferenceContext _localctx = new ReferenceContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_reference);
		try {
			setState(230);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Reference:
				enterOuterAlt(_localctx, 1);
				{
				setState(224);
				match(Reference);
				}
				break;
			case ReferenceIndicator:
				enterOuterAlt(_localctx, 2);
				{
				setState(225);
				match(ReferenceIndicator);
				setState(226);
				match(OpenBracket);
				setState(227);
				value(0);
				setState(228);
				match(ClosedBracket);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(MKSONParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MKSONParser.ID, i);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitFunctionCall(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_functionCall);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
				setState(232);
				match(ID);
				setState(233);
				match(DoubleColon);
				}
				break;
			}
			setState(236);
			match(ID);
			setState(237);
			match(OpenParen);
			setState(243);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(238);
					value(0);
					setState(239);
					match(Comma);
					}
					} 
				}
				setState(245);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			setState(247);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KeywordIf) | (1L << KeywordTrue) | (1L << KeywordFalse) | (1L << KeywordNull) | (1L << Reference) | (1L << ReferenceIndicator) | (1L << ID) | (1L << String) | (1L << Number) | (1L << OpenBrace) | (1L << OpenBracket) | (1L << OpenParen) | (1L << ExclamationMark) | (1L << Dash) | (1L << QuestionMark))) != 0)) {
				{
				setState(246);
				value(0);
				}
			}

			setState(249);
			match(ClosedParen);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InheritanceContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public InheritanceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inheritance; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterInheritance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitInheritance(this);
		}
	}

	public final InheritanceContext inheritance() throws RecognitionException {
		InheritanceContext _localctx = new InheritanceContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_inheritance);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(251);
			match(OpenBrace);
			setState(252);
			value(0);
			setState(253);
			match(ClosedBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectContext extends ParserRuleContext {
		public List<PairContext> pair() {
			return getRuleContexts(PairContext.class);
		}
		public PairContext pair(int i) {
			return getRuleContext(PairContext.class,i);
		}
		public ObjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_object; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterObject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitObject(this);
		}
	}

	public final ObjectContext object() throws RecognitionException {
		ObjectContext _localctx = new ObjectContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_object);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			match(OpenBrace);
			setState(268);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ID) | (1L << String) | (1L << Asterisk))) != 0)) {
				{
				setState(261);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(256);
						pair();
						setState(257);
						match(Comma);
						}
						} 
					}
					setState(263);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				}
				setState(264);
				pair();
				setState(266);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(265);
					match(Comma);
					}
				}

				}
			}

			setState(270);
			match(ClosedBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PairContext extends ParserRuleContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public TerminalNode ID() { return getToken(MKSONParser.ID, 0); }
		public TerminalNode String() { return getToken(MKSONParser.String, 0); }
		public TerminalNode Asterisk() { return getToken(MKSONParser.Asterisk, 0); }
		public PairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterPair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitPair(this);
		}
	}

	public final PairContext pair() throws RecognitionException {
		PairContext _localctx = new PairContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_pair);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(273);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Asterisk) {
				{
				setState(272);
				match(Asterisk);
				}
			}

			setState(275);
			_la = _input.LA(1);
			if ( !(_la==ID || _la==String) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(276);
			_la = _input.LA(1);
			if ( !(_la==Assignment || _la==Colon) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(277);
			value(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayContext extends ParserRuleContext {
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public ArrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitArray(this);
		}
	}

	public final ArrayContext array() throws RecognitionException {
		ArrayContext _localctx = new ArrayContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_array);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			match(OpenBracket);
			setState(292);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KeywordIf) | (1L << KeywordTrue) | (1L << KeywordFalse) | (1L << KeywordNull) | (1L << Reference) | (1L << ReferenceIndicator) | (1L << ID) | (1L << String) | (1L << Number) | (1L << OpenBrace) | (1L << OpenBracket) | (1L << OpenParen) | (1L << ExclamationMark) | (1L << Dash) | (1L << QuestionMark))) != 0)) {
				{
				setState(285);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(280);
						value(0);
						setState(281);
						match(Comma);
						}
						} 
					}
					setState(287);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				}
				setState(288);
				value(0);
				setState(290);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(289);
					match(Comma);
					}
				}

				}
			}

			setState(294);
			match(ClosedBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(MKSONParser.String, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(296);
			match(String);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public TerminalNode Number() { return getToken(MKSONParser.Number, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitNumber(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_number);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(298);
			match(Number);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitBool(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_bool);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			_la = _input.LA(1);
			if ( !(_la==KeywordTrue || _la==KeywordFalse) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NulltypeContext extends ParserRuleContext {
		public NulltypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nulltype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterNulltype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitNulltype(this);
		}
	}

	public final NulltypeContext nulltype() throws RecognitionException {
		NulltypeContext _localctx = new NulltypeContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_nulltype);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			match(KeywordNull);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UndefinedContext extends ParserRuleContext {
		public UndefinedContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_undefined; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).enterUndefined(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MKSONParserListener ) ((MKSONParserListener)listener).exitUndefined(this);
		}
	}

	public final UndefinedContext undefined() throws RecognitionException {
		UndefinedContext _localctx = new UndefinedContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_undefined);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(304);
			match(QuestionMark);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8:
			return value_sempred((ValueContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean value_sempred(ValueContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		case 4:
			return precpred(_ctx, 4);
		case 5:
			return precpred(_ctx, 3);
		case 6:
			return precpred(_ctx, 2);
		case 7:
			return precpred(_ctx, 1);
		case 8:
			return precpred(_ctx, 16);
		case 9:
			return precpred(_ctx, 15);
		case 10:
			return precpred(_ctx, 14);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\64\u0135\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\7\2.\n\2\f\2\16\2\61\13\2"+
		"\3\2\3\2\7\2\65\n\2\f\2\16\28\13\2\3\3\3\3\3\3\5\3=\n\3\3\4\3\4\3\4\3"+
		"\4\3\4\5\4D\n\4\3\4\3\4\7\4H\n\4\f\4\16\4K\13\4\3\4\3\4\3\4\5\4P\n\4\3"+
		"\4\5\4S\n\4\5\4U\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\7\5^\n\5\f\5\16\5a\13"+
		"\5\3\5\3\5\5\5e\n\5\5\5g\n\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\5\6p\n\6\3\6"+
		"\3\6\7\6t\n\6\f\6\16\6w\13\6\3\6\3\6\3\6\5\6|\n\6\3\6\5\6\177\n\6\5\6"+
		"\u0081\n\6\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00a2"+
		"\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\7\n\u00c7\n\n\f\n\16\n\u00ca\13\n\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\5\13\u00d5\n\13\6\13\u00d7\n\13\r\13\16\13\u00d8\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\5\13\u00e1\n\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00e9"+
		"\n\f\3\r\3\r\5\r\u00ed\n\r\3\r\3\r\3\r\3\r\3\r\7\r\u00f4\n\r\f\r\16\r"+
		"\u00f7\13\r\3\r\5\r\u00fa\n\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3"+
		"\17\3\17\7\17\u0106\n\17\f\17\16\17\u0109\13\17\3\17\3\17\5\17\u010d\n"+
		"\17\5\17\u010f\n\17\3\17\3\17\3\20\5\20\u0114\n\20\3\20\3\20\3\20\3\20"+
		"\3\21\3\21\3\21\3\21\7\21\u011e\n\21\f\21\16\21\u0121\13\21\3\21\3\21"+
		"\5\21\u0125\n\21\5\21\u0127\n\21\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3"+
		"\24\3\25\3\25\3\26\3\26\3\26\2\3\22\27\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*\2\t\3\2\"$\4\2  %%\3\2\b\t\3\2&+\3\2\24\25\3\2/\60\3\2\13"+
		"\f\u0156\2/\3\2\2\2\4<\3\2\2\2\6>\3\2\2\2\bX\3\2\2\2\nj\3\2\2\2\f\u0084"+
		"\3\2\2\2\16\u0086\3\2\2\2\20\u0088\3\2\2\2\22\u00a1\3\2\2\2\24\u00cb\3"+
		"\2\2\2\26\u00e8\3\2\2\2\30\u00ec\3\2\2\2\32\u00fd\3\2\2\2\34\u0101\3\2"+
		"\2\2\36\u0113\3\2\2\2 \u0119\3\2\2\2\"\u012a\3\2\2\2$\u012c\3\2\2\2&\u012e"+
		"\3\2\2\2(\u0130\3\2\2\2*\u0132\3\2\2\2,.\5\4\3\2-,\3\2\2\2.\61\3\2\2\2"+
		"/-\3\2\2\2/\60\3\2\2\2\60\62\3\2\2\2\61/\3\2\2\2\62\66\5\22\n\2\63\65"+
		"\5\4\3\2\64\63\3\2\2\2\658\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\67\3\3"+
		"\2\2\28\66\3\2\2\29=\5\6\4\2:=\5\b\5\2;=\5\n\6\2<9\3\2\2\2<:\3\2\2\2<"+
		";\3\2\2\2=\5\3\2\2\2>?\7\3\2\2?T\7\31\2\2@C\5\f\7\2AB\7\6\2\2BD\5\16\b"+
		"\2CA\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7.\2\2FH\3\2\2\2G@\3\2\2\2HK\3\2\2"+
		"\2IG\3\2\2\2IJ\3\2\2\2JL\3\2\2\2KI\3\2\2\2LO\5\f\7\2MN\7\6\2\2NP\5\16"+
		"\b\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QS\7.\2\2RQ\3\2\2\2RS\3\2\2\2SU\3\2"+
		"\2\2TI\3\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7\32\2\2W\7\3\2\2\2XY\7\4\2\2Yf\7"+
		"\31\2\2Z[\5\20\t\2[\\\7.\2\2\\^\3\2\2\2]Z\3\2\2\2^a\3\2\2\2_]\3\2\2\2"+
		"_`\3\2\2\2`b\3\2\2\2a_\3\2\2\2bd\5\20\t\2ce\7.\2\2dc\3\2\2\2de\3\2\2\2"+
		"eg\3\2\2\2f_\3\2\2\2fg\3\2\2\2gh\3\2\2\2hi\7\32\2\2i\t\3\2\2\2jk\7\5\2"+
		"\2k\u0080\7\31\2\2lo\5\f\7\2mn\7\6\2\2np\5\16\b\2om\3\2\2\2op\3\2\2\2"+
		"pq\3\2\2\2qr\7.\2\2rt\3\2\2\2sl\3\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2"+
		"vx\3\2\2\2wu\3\2\2\2x{\5\f\7\2yz\7\6\2\2z|\5\16\b\2{y\3\2\2\2{|\3\2\2"+
		"\2|~\3\2\2\2}\177\7.\2\2~}\3\2\2\2~\177\3\2\2\2\177\u0081\3\2\2\2\u0080"+
		"u\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0083\7\32\2\2"+
		"\u0083\13\3\2\2\2\u0084\u0085\7\25\2\2\u0085\r\3\2\2\2\u0086\u0087\7\24"+
		"\2\2\u0087\17\3\2\2\2\u0088\u0089\7\24\2\2\u0089\21\3\2\2\2\u008a\u008b"+
		"\b\n\1\2\u008b\u00a2\5\"\22\2\u008c\u00a2\5$\23\2\u008d\u00a2\5&\24\2"+
		"\u008e\u00a2\5(\25\2\u008f\u00a2\5*\26\2\u0090\u00a2\5\34\17\2\u0091\u0092"+
		"\5\32\16\2\u0092\u0093\7\61\2\2\u0093\u0094\5\34\17\2\u0094\u00a2\3\2"+
		"\2\2\u0095\u00a2\5 \21\2\u0096\u00a2\5\26\f\2\u0097\u00a2\5\30\r\2\u0098"+
		"\u0099\7\35\2\2\u0099\u009a\5\22\n\2\u009a\u009b\7\36\2\2\u009b\u00a2"+
		"\3\2\2\2\u009c\u00a2\5\24\13\2\u009d\u009e\7\37\2\2\u009e\u00a2\5\22\n"+
		"\f\u009f\u00a0\7 \2\2\u00a0\u00a2\5\22\n\13\u00a1\u008a\3\2\2\2\u00a1"+
		"\u008c\3\2\2\2\u00a1\u008d\3\2\2\2\u00a1\u008e\3\2\2\2\u00a1\u008f\3\2"+
		"\2\2\u00a1\u0090\3\2\2\2\u00a1\u0091\3\2\2\2\u00a1\u0095\3\2\2\2\u00a1"+
		"\u0096\3\2\2\2\u00a1\u0097\3\2\2\2\u00a1\u0098\3\2\2\2\u00a1\u009c\3\2"+
		"\2\2\u00a1\u009d\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00c8\3\2\2\2\u00a3"+
		"\u00a4\f\n\2\2\u00a4\u00a5\7!\2\2\u00a5\u00c7\5\22\n\13\u00a6\u00a7\f"+
		"\t\2\2\u00a7\u00a8\7\30\2\2\u00a8\u00c7\5\22\n\n\u00a9\u00aa\f\b\2\2\u00aa"+
		"\u00ab\t\2\2\2\u00ab\u00c7\5\22\n\t\u00ac\u00ad\f\7\2\2\u00ad\u00ae\t"+
		"\3\2\2\u00ae\u00c7\5\22\n\b\u00af\u00b0\f\6\2\2\u00b0\u00b1\t\4\2\2\u00b1"+
		"\u00c7\5\22\n\7\u00b2\u00b3\f\5\2\2\u00b3\u00b4\t\5\2\2\u00b4\u00c7\5"+
		"\22\n\6\u00b5\u00b6\f\4\2\2\u00b6\u00b7\7,\2\2\u00b7\u00c7\5\22\n\5\u00b8"+
		"\u00b9\f\3\2\2\u00b9\u00ba\7-\2\2\u00ba\u00c7\5\22\n\4\u00bb\u00bc\f\22"+
		"\2\2\u00bc\u00bd\7\27\2\2\u00bd\u00c7\5\30\r\2\u00be\u00bf\f\21\2\2\u00bf"+
		"\u00c0\7\27\2\2\u00c0\u00c7\7\24\2\2\u00c1\u00c2\f\20\2\2\u00c2\u00c3"+
		"\7\33\2\2\u00c3\u00c4\5\22\n\2\u00c4\u00c5\7\34\2\2\u00c5\u00c7\3\2\2"+
		"\2\u00c6\u00a3\3\2\2\2\u00c6\u00a6\3\2\2\2\u00c6\u00a9\3\2\2\2\u00c6\u00ac"+
		"\3\2\2\2\u00c6\u00af\3\2\2\2\u00c6\u00b2\3\2\2\2\u00c6\u00b5\3\2\2\2\u00c6"+
		"\u00b8\3\2\2\2\u00c6\u00bb\3\2\2\2\u00c6\u00be\3\2\2\2\u00c6\u00c1\3\2"+
		"\2\2\u00c7\u00ca\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9"+
		"\23\3\2\2\2\u00ca\u00c8\3\2\2\2\u00cb\u00d6\7\7\2\2\u00cc\u00cd\7\35\2"+
		"\2\u00cd\u00ce\5\22\n\2\u00ce\u00d4\7\36\2\2\u00cf\u00d0\7\31\2\2\u00d0"+
		"\u00d1\5\22\n\2\u00d1\u00d2\7\32\2\2\u00d2\u00d5\3\2\2\2\u00d3\u00d5\5"+
		"\22\n\2\u00d4\u00cf\3\2\2\2\u00d4\u00d3\3\2\2\2\u00d5\u00d7\3\2\2\2\u00d6"+
		"\u00cc\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d8\u00d9\3\2"+
		"\2\2\u00d9\u00da\3\2\2\2\u00da\u00e0\7\n\2\2\u00db\u00dc\7\31\2\2\u00dc"+
		"\u00dd\5\22\n\2\u00dd\u00de\7\32\2\2\u00de\u00e1\3\2\2\2\u00df\u00e1\5"+
		"\22\n\2\u00e0\u00db\3\2\2\2\u00e0\u00df\3\2\2\2\u00e1\25\3\2\2\2\u00e2"+
		"\u00e9\7\20\2\2\u00e3\u00e4\7\21\2\2\u00e4\u00e5\7\33\2\2\u00e5\u00e6"+
		"\5\22\n\2\u00e6\u00e7\7\34\2\2\u00e7\u00e9\3\2\2\2\u00e8\u00e2\3\2\2\2"+
		"\u00e8\u00e3\3\2\2\2\u00e9\27\3\2\2\2\u00ea\u00eb\7\24\2\2\u00eb\u00ed"+
		"\7\61\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00ee\3\2\2\2"+
		"\u00ee\u00ef\7\24\2\2\u00ef\u00f5\7\35\2\2\u00f0\u00f1\5\22\n\2\u00f1"+
		"\u00f2\7.\2\2\u00f2\u00f4\3\2\2\2\u00f3\u00f0\3\2\2\2\u00f4\u00f7\3\2"+
		"\2\2\u00f5\u00f3\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f9\3\2\2\2\u00f7"+
		"\u00f5\3\2\2\2\u00f8\u00fa\5\22\n\2\u00f9\u00f8\3\2\2\2\u00f9\u00fa\3"+
		"\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fc\7\36\2\2\u00fc\31\3\2\2\2\u00fd"+
		"\u00fe\7\31\2\2\u00fe\u00ff\5\22\n\2\u00ff\u0100\7\32\2\2\u0100\33\3\2"+
		"\2\2\u0101\u010e\7\31\2\2\u0102\u0103\5\36\20\2\u0103\u0104\7.\2\2\u0104"+
		"\u0106\3\2\2\2\u0105\u0102\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2"+
		"\2\2\u0107\u0108\3\2\2\2\u0108\u010a\3\2\2\2\u0109\u0107\3\2\2\2\u010a"+
		"\u010c\5\36\20\2\u010b\u010d\7.\2\2\u010c\u010b\3\2\2\2\u010c\u010d\3"+
		"\2\2\2\u010d\u010f\3\2\2\2\u010e\u0107\3\2\2\2\u010e\u010f\3\2\2\2\u010f"+
		"\u0110\3\2\2\2\u0110\u0111\7\32\2\2\u0111\35\3\2\2\2\u0112\u0114\7\30"+
		"\2\2\u0113\u0112\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\3\2\2\2\u0115"+
		"\u0116\t\6\2\2\u0116\u0117\t\7\2\2\u0117\u0118\5\22\n\2\u0118\37\3\2\2"+
		"\2\u0119\u0126\7\33\2\2\u011a\u011b\5\22\n\2\u011b\u011c\7.\2\2\u011c"+
		"\u011e\3\2\2\2\u011d\u011a\3\2\2\2\u011e\u0121\3\2\2\2\u011f\u011d\3\2"+
		"\2\2\u011f\u0120\3\2\2\2\u0120\u0122\3\2\2\2\u0121\u011f\3\2\2\2\u0122"+
		"\u0124\5\22\n\2\u0123\u0125\7.\2\2\u0124\u0123\3\2\2\2\u0124\u0125\3\2"+
		"\2\2\u0125\u0127\3\2\2\2\u0126\u011f\3\2\2\2\u0126\u0127\3\2\2\2\u0127"+
		"\u0128\3\2\2\2\u0128\u0129\7\34\2\2\u0129!\3\2\2\2\u012a\u012b\7\25\2"+
		"\2\u012b#\3\2\2\2\u012c\u012d\7\26\2\2\u012d%\3\2\2\2\u012e\u012f\t\b"+
		"\2\2\u012f\'\3\2\2\2\u0130\u0131\7\r\2\2\u0131)\3\2\2\2\u0132\u0133\7"+
		"\63\2\2\u0133+\3\2\2\2#/\66<CIORT_dfou{~\u0080\u00a1\u00c6\u00c8\u00d4"+
		"\u00d8\u00e0\u00e8\u00ec\u00f5\u00f9\u0107\u010c\u010e\u0113\u011f\u0124"+
		"\u0126";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}