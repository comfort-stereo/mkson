// Generated from MKSONLexer.g4 by ANTLR 4.6

package antlrgen;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MKSONLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		KeywordImport=1, KeywordModule=2, KeywordScript=3, KeywordAs=4, KeywordIf=5, 
		KeywordIn=6, KeywordNotIn=7, KeywordElse=8, KeywordTrue=9, KeywordFalse=10, 
		KeywordNull=11, Comment=12, LineComment=13, Reference=14, ReferenceIndicator=15, 
		ModuleReferenceIndicator=16, SelfReferenceIndicator=17, ID=18, String=19, 
		Number=20, Dot=21, Asterisk=22, OpenBrace=23, ClosedBrace=24, OpenBracket=25, 
		ClosedBracket=26, OpenParen=27, ClosedParen=28, ExclamationMark=29, Dash=30, 
		Caret=31, Slash=32, Backslash=33, Modulus=34, Plus=35, Equals=36, NotEquals=37, 
		GreaterThan=38, LessThan=39, GreaterThanOrEqual=40, LessThanOrEqual=41, 
		And=42, Or=43, Comma=44, Assignment=45, Colon=46, DoubleColon=47, Quote=48, 
		QuestionMark=49, WS=50;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"KeywordImport", "KeywordModule", "KeywordScript", "KeywordAs", "KeywordIf", 
		"KeywordIn", "KeywordNotIn", "KeywordElse", "KeywordTrue", "KeywordFalse", 
		"KeywordNull", "Comment", "LineComment", "Reference", "ReferenceIndicator", 
		"ModuleReferenceIndicator", "SelfReferenceIndicator", "ID", "String", 
		"ESC", "UNICODE", "HEX", "Number", "INT", "HEXINT", "EXP", "Dot", "Asterisk", 
		"OpenBrace", "ClosedBrace", "OpenBracket", "ClosedBracket", "OpenParen", 
		"ClosedParen", "ExclamationMark", "Dash", "Caret", "Slash", "Backslash", 
		"Modulus", "Plus", "Equals", "NotEquals", "GreaterThan", "LessThan", "GreaterThanOrEqual", 
		"LessThanOrEqual", "And", "Or", "Comma", "Assignment", "Colon", "DoubleColon", 
		"Quote", "QuestionMark", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'import'", "'module'", "'script'", "'as'", "'if'", "'in'", "'!in'", 
		"'else'", "'true'", "'false'", "'null'", null, null, null, null, "'@'", 
		null, null, null, null, "'.'", "'*'", "'{'", "'}'", "'['", "']'", "'('", 
		"')'", "'!'", "'-'", "'^'", "'/'", "'\\'", "'%'", "'+'", "'=='", "'!='", 
		"'>'", "'<'", "'>='", "'<='", "'&&'", "'||'", "','", "'='", "':'", "'::'", 
		"'\"'", "'?'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "KeywordImport", "KeywordModule", "KeywordScript", "KeywordAs", 
		"KeywordIf", "KeywordIn", "KeywordNotIn", "KeywordElse", "KeywordTrue", 
		"KeywordFalse", "KeywordNull", "Comment", "LineComment", "Reference", 
		"ReferenceIndicator", "ModuleReferenceIndicator", "SelfReferenceIndicator", 
		"ID", "String", "Number", "Dot", "Asterisk", "OpenBrace", "ClosedBrace", 
		"OpenBracket", "ClosedBracket", "OpenParen", "ClosedParen", "ExclamationMark", 
		"Dash", "Caret", "Slash", "Backslash", "Modulus", "Plus", "Equals", "NotEquals", 
		"GreaterThan", "LessThan", "GreaterThanOrEqual", "LessThanOrEqual", "And", 
		"Or", "Comma", "Assignment", "Colon", "DoubleColon", "Quote", "QuestionMark", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MKSONLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MKSONLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\64\u0169\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5"+
		"\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3"+
		"\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3"+
		"\r\3\r\3\r\3\r\7\r\u00b0\n\r\f\r\16\r\u00b3\13\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\16\3\16\3\16\3\16\7\16\u00be\n\16\f\16\16\16\u00c1\13\16\3\16\3\16\3"+
		"\16\3\16\3\17\3\17\3\17\3\20\3\20\5\20\u00cc\n\20\3\21\3\21\3\22\6\22"+
		"\u00d1\n\22\r\22\16\22\u00d2\3\22\7\22\u00d6\n\22\f\22\16\22\u00d9\13"+
		"\22\3\23\3\23\7\23\u00dd\n\23\f\23\16\23\u00e0\13\23\3\24\3\24\3\24\7"+
		"\24\u00e5\n\24\f\24\16\24\u00e8\13\24\3\24\3\24\3\25\3\25\3\25\5\25\u00ef"+
		"\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\30\3\30\3\30\3\30\6\30"+
		"\u00fd\n\30\r\30\16\30\u00fe\3\30\5\30\u0102\n\30\3\30\3\30\3\30\3\30"+
		"\5\30\u0108\n\30\5\30\u010a\n\30\3\31\3\31\3\31\7\31\u010f\n\31\f\31\16"+
		"\31\u0112\13\31\5\31\u0114\n\31\3\32\3\32\6\32\u0118\n\32\r\32\16\32\u0119"+
		"\3\33\3\33\5\33\u011e\n\33\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37"+
		"\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)"+
		"\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3.\3.\3/\3/\3/\3\60\3\60\3\60\3\61\3\61"+
		"\3\61\3\62\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\66\3\67"+
		"\3\67\38\38\39\69\u0164\n9\r9\169\u0165\39\39\4\u00b1\u00bf\2:\3\3\5\4"+
		"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22"+
		"#\23%\24\'\25)\2+\2-\2/\26\61\2\63\2\65\2\67\279\30;\31=\32?\33A\34C\35"+
		"E\36G\37I K!M\"O#Q$S%U&W\'Y([)]*_+a,c-e.g/i\60k\61m\62o\63q\64\3\2\f\5"+
		"\2C\\aac|\6\2\62;C\\aac|\4\2$$^^\13\2$$&&\61\61^^ddhhppttvv\5\2\62;CH"+
		"ch\3\2\62;\3\2\63;\4\2GGgg\4\2--//\5\2\13\f\17\17\"\"\u0176\2\3\3\2\2"+
		"\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3"+
		"\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2"+
		"\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2"+
		"\2\2\2\'\3\2\2\2\2/\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2"+
		"\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2"+
		"\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W"+
		"\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2"+
		"\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2"+
		"\2q\3\2\2\2\3s\3\2\2\2\5z\3\2\2\2\7\u0081\3\2\2\2\t\u0088\3\2\2\2\13\u008b"+
		"\3\2\2\2\r\u008e\3\2\2\2\17\u0091\3\2\2\2\21\u0095\3\2\2\2\23\u009a\3"+
		"\2\2\2\25\u009f\3\2\2\2\27\u00a5\3\2\2\2\31\u00aa\3\2\2\2\33\u00b9\3\2"+
		"\2\2\35\u00c6\3\2\2\2\37\u00cb\3\2\2\2!\u00cd\3\2\2\2#\u00d0\3\2\2\2%"+
		"\u00da\3\2\2\2\'\u00e1\3\2\2\2)\u00eb\3\2\2\2+\u00f0\3\2\2\2-\u00f6\3"+
		"\2\2\2/\u0109\3\2\2\2\61\u0113\3\2\2\2\63\u0115\3\2\2\2\65\u011b\3\2\2"+
		"\2\67\u0121\3\2\2\29\u0123\3\2\2\2;\u0125\3\2\2\2=\u0127\3\2\2\2?\u0129"+
		"\3\2\2\2A\u012b\3\2\2\2C\u012d\3\2\2\2E\u012f\3\2\2\2G\u0131\3\2\2\2I"+
		"\u0133\3\2\2\2K\u0135\3\2\2\2M\u0137\3\2\2\2O\u0139\3\2\2\2Q\u013b\3\2"+
		"\2\2S\u013d\3\2\2\2U\u013f\3\2\2\2W\u0142\3\2\2\2Y\u0145\3\2\2\2[\u0147"+
		"\3\2\2\2]\u0149\3\2\2\2_\u014c\3\2\2\2a\u014f\3\2\2\2c\u0152\3\2\2\2e"+
		"\u0155\3\2\2\2g\u0157\3\2\2\2i\u0159\3\2\2\2k\u015b\3\2\2\2m\u015e\3\2"+
		"\2\2o\u0160\3\2\2\2q\u0163\3\2\2\2st\7k\2\2tu\7o\2\2uv\7r\2\2vw\7q\2\2"+
		"wx\7t\2\2xy\7v\2\2y\4\3\2\2\2z{\7o\2\2{|\7q\2\2|}\7f\2\2}~\7w\2\2~\177"+
		"\7n\2\2\177\u0080\7g\2\2\u0080\6\3\2\2\2\u0081\u0082\7u\2\2\u0082\u0083"+
		"\7e\2\2\u0083\u0084\7t\2\2\u0084\u0085\7k\2\2\u0085\u0086\7r\2\2\u0086"+
		"\u0087\7v\2\2\u0087\b\3\2\2\2\u0088\u0089\7c\2\2\u0089\u008a\7u\2\2\u008a"+
		"\n\3\2\2\2\u008b\u008c\7k\2\2\u008c\u008d\7h\2\2\u008d\f\3\2\2\2\u008e"+
		"\u008f\7k\2\2\u008f\u0090\7p\2\2\u0090\16\3\2\2\2\u0091\u0092\7#\2\2\u0092"+
		"\u0093\7k\2\2\u0093\u0094\7p\2\2\u0094\20\3\2\2\2\u0095\u0096\7g\2\2\u0096"+
		"\u0097\7n\2\2\u0097\u0098\7u\2\2\u0098\u0099\7g\2\2\u0099\22\3\2\2\2\u009a"+
		"\u009b\7v\2\2\u009b\u009c\7t\2\2\u009c\u009d\7w\2\2\u009d\u009e\7g\2\2"+
		"\u009e\24\3\2\2\2\u009f\u00a0\7h\2\2\u00a0\u00a1\7c\2\2\u00a1\u00a2\7"+
		"n\2\2\u00a2\u00a3\7u\2\2\u00a3\u00a4\7g\2\2\u00a4\26\3\2\2\2\u00a5\u00a6"+
		"\7p\2\2\u00a6\u00a7\7w\2\2\u00a7\u00a8\7n\2\2\u00a8\u00a9\7n\2\2\u00a9"+
		"\30\3\2\2\2\u00aa\u00ab\7\61\2\2\u00ab\u00ac\7,\2\2\u00ac\u00b1\3\2\2"+
		"\2\u00ad\u00b0\5\31\r\2\u00ae\u00b0\13\2\2\2\u00af\u00ad\3\2\2\2\u00af"+
		"\u00ae\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b1\u00af\3\2"+
		"\2\2\u00b2\u00b4\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b4\u00b5\7,\2\2\u00b5"+
		"\u00b6\7\61\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\b\r\2\2\u00b8\32\3\2\2"+
		"\2\u00b9\u00ba\7\61\2\2\u00ba\u00bb\7\61\2\2\u00bb\u00bf\3\2\2\2\u00bc"+
		"\u00be\13\2\2\2\u00bd\u00bc\3\2\2\2\u00be\u00c1\3\2\2\2\u00bf\u00c0\3"+
		"\2\2\2\u00bf\u00bd\3\2\2\2\u00c0\u00c2\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c2"+
		"\u00c3\7\f\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5\b\16\2\2\u00c5\34\3\2\2"+
		"\2\u00c6\u00c7\5\37\20\2\u00c7\u00c8\5%\23\2\u00c8\36\3\2\2\2\u00c9\u00cc"+
		"\5!\21\2\u00ca\u00cc\5#\22\2\u00cb\u00c9\3\2\2\2\u00cb\u00ca\3\2\2\2\u00cc"+
		" \3\2\2\2\u00cd\u00ce\7B\2\2\u00ce\"\3\2\2\2\u00cf\u00d1\7&\2\2\u00d0"+
		"\u00cf\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2"+
		"\2\2\u00d3\u00d7\3\2\2\2\u00d4\u00d6\7-\2\2\u00d5\u00d4\3\2\2\2\u00d6"+
		"\u00d9\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8$\3\2\2\2"+
		"\u00d9\u00d7\3\2\2\2\u00da\u00de\t\2\2\2\u00db\u00dd\t\3\2\2\u00dc\u00db"+
		"\3\2\2\2\u00dd\u00e0\3\2\2\2\u00de\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"&\3\2\2\2\u00e0\u00de\3\2\2\2\u00e1\u00e6\5m\67\2\u00e2\u00e5\5)\25\2"+
		"\u00e3\u00e5\n\4\2\2\u00e4\u00e2\3\2\2\2\u00e4\u00e3\3\2\2\2\u00e5\u00e8"+
		"\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e9\3\2\2\2\u00e8"+
		"\u00e6\3\2\2\2\u00e9\u00ea\5m\67\2\u00ea(\3\2\2\2\u00eb\u00ee\7^\2\2\u00ec"+
		"\u00ef\t\5\2\2\u00ed\u00ef\5+\26\2\u00ee\u00ec\3\2\2\2\u00ee\u00ed\3\2"+
		"\2\2\u00ef*\3\2\2\2\u00f0\u00f1\7w\2\2\u00f1\u00f2\5-\27\2\u00f2\u00f3"+
		"\5-\27\2\u00f3\u00f4\5-\27\2\u00f4\u00f5\5-\27\2\u00f5,\3\2\2\2\u00f6"+
		"\u00f7\t\6\2\2\u00f7.\3\2\2\2\u00f8\u010a\5\63\32\2\u00f9\u00fa\5\61\31"+
		"\2\u00fa\u00fc\7\60\2\2\u00fb\u00fd\t\7\2\2\u00fc\u00fb\3\2\2\2\u00fd"+
		"\u00fe\3\2\2\2\u00fe\u00fc\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u0101\3\2"+
		"\2\2\u0100\u0102\5\65\33\2\u0101\u0100\3\2\2\2\u0101\u0102\3\2\2\2\u0102"+
		"\u0108\3\2\2\2\u0103\u0104\5\61\31\2\u0104\u0105\5\65\33\2\u0105\u0108"+
		"\3\2\2\2\u0106\u0108\5\61\31\2\u0107\u00f9\3\2\2\2\u0107\u0103\3\2\2\2"+
		"\u0107\u0106\3\2\2\2\u0108\u010a\3\2\2\2\u0109\u00f8\3\2\2\2\u0109\u0107"+
		"\3\2\2\2\u010a\60\3\2\2\2\u010b\u0114\7\62\2\2\u010c\u0110\t\b\2\2\u010d"+
		"\u010f\t\7\2\2\u010e\u010d\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2"+
		"\2\2\u0110\u0111\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110\3\2\2\2\u0113"+
		"\u010b\3\2\2\2\u0113\u010c\3\2\2\2\u0114\62\3\2\2\2\u0115\u0117\7%\2\2"+
		"\u0116\u0118\t\6\2\2\u0117\u0116\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u0117"+
		"\3\2\2\2\u0119\u011a\3\2\2\2\u011a\64\3\2\2\2\u011b\u011d\t\t\2\2\u011c"+
		"\u011e\t\n\2\2\u011d\u011c\3\2\2\2\u011d\u011e\3\2\2\2\u011e\u011f\3\2"+
		"\2\2\u011f\u0120\5\61\31\2\u0120\66\3\2\2\2\u0121\u0122\7\60\2\2\u0122"+
		"8\3\2\2\2\u0123\u0124\7,\2\2\u0124:\3\2\2\2\u0125\u0126\7}\2\2\u0126<"+
		"\3\2\2\2\u0127\u0128\7\177\2\2\u0128>\3\2\2\2\u0129\u012a\7]\2\2\u012a"+
		"@\3\2\2\2\u012b\u012c\7_\2\2\u012cB\3\2\2\2\u012d\u012e\7*\2\2\u012eD"+
		"\3\2\2\2\u012f\u0130\7+\2\2\u0130F\3\2\2\2\u0131\u0132\7#\2\2\u0132H\3"+
		"\2\2\2\u0133\u0134\7/\2\2\u0134J\3\2\2\2\u0135\u0136\7`\2\2\u0136L\3\2"+
		"\2\2\u0137\u0138\7\61\2\2\u0138N\3\2\2\2\u0139\u013a\7^\2\2\u013aP\3\2"+
		"\2\2\u013b\u013c\7\'\2\2\u013cR\3\2\2\2\u013d\u013e\7-\2\2\u013eT\3\2"+
		"\2\2\u013f\u0140\7?\2\2\u0140\u0141\7?\2\2\u0141V\3\2\2\2\u0142\u0143"+
		"\7#\2\2\u0143\u0144\7?\2\2\u0144X\3\2\2\2\u0145\u0146\7@\2\2\u0146Z\3"+
		"\2\2\2\u0147\u0148\7>\2\2\u0148\\\3\2\2\2\u0149\u014a\7@\2\2\u014a\u014b"+
		"\7?\2\2\u014b^\3\2\2\2\u014c\u014d\7>\2\2\u014d\u014e\7?\2\2\u014e`\3"+
		"\2\2\2\u014f\u0150\7(\2\2\u0150\u0151\7(\2\2\u0151b\3\2\2\2\u0152\u0153"+
		"\7~\2\2\u0153\u0154\7~\2\2\u0154d\3\2\2\2\u0155\u0156\7.\2\2\u0156f\3"+
		"\2\2\2\u0157\u0158\7?\2\2\u0158h\3\2\2\2\u0159\u015a\7<\2\2\u015aj\3\2"+
		"\2\2\u015b\u015c\7<\2\2\u015c\u015d\7<\2\2\u015dl\3\2\2\2\u015e\u015f"+
		"\7$\2\2\u015fn\3\2\2\2\u0160\u0161\7A\2\2\u0161p\3\2\2\2\u0162\u0164\t"+
		"\13\2\2\u0163\u0162\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0163\3\2\2\2\u0165"+
		"\u0166\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0168\b9\2\2\u0168r\3\2\2\2\26"+
		"\2\u00af\u00b1\u00bf\u00cb\u00d2\u00d7\u00de\u00e4\u00e6\u00ee\u00fe\u0101"+
		"\u0107\u0109\u0110\u0113\u0119\u011d\u0165\3\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}