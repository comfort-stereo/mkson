// Generated from MKSONParser.g4 by ANTLR 4.6

package antlrgen;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MKSONParser}.
 */
public interface MKSONParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MKSONParser#mkson}.
	 * @param ctx the parse tree
	 */
	void enterMkson(MKSONParser.MksonContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#mkson}.
	 * @param ctx the parse tree
	 */
	void exitMkson(MKSONParser.MksonContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#meta}.
	 * @param ctx the parse tree
	 */
	void enterMeta(MKSONParser.MetaContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#meta}.
	 * @param ctx the parse tree
	 */
	void exitMeta(MKSONParser.MetaContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(MKSONParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(MKSONParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#directiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterDirectiveStatement(MKSONParser.DirectiveStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#directiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitDirectiveStatement(MKSONParser.DirectiveStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#scriptStatement}.
	 * @param ctx the parse tree
	 */
	void enterScriptStatement(MKSONParser.ScriptStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#scriptStatement}.
	 * @param ctx the parse tree
	 */
	void exitScriptStatement(MKSONParser.ScriptStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#path}.
	 * @param ctx the parse tree
	 */
	void enterPath(MKSONParser.PathContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#path}.
	 * @param ctx the parse tree
	 */
	void exitPath(MKSONParser.PathContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#alias}.
	 * @param ctx the parse tree
	 */
	void enterAlias(MKSONParser.AliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#alias}.
	 * @param ctx the parse tree
	 */
	void exitAlias(MKSONParser.AliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#directive}.
	 * @param ctx the parse tree
	 */
	void enterDirective(MKSONParser.DirectiveContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#directive}.
	 * @param ctx the parse tree
	 */
	void exitDirective(MKSONParser.DirectiveContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(MKSONParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(MKSONParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(MKSONParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(MKSONParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(MKSONParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(MKSONParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(MKSONParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(MKSONParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#inheritance}.
	 * @param ctx the parse tree
	 */
	void enterInheritance(MKSONParser.InheritanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#inheritance}.
	 * @param ctx the parse tree
	 */
	void exitInheritance(MKSONParser.InheritanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#object}.
	 * @param ctx the parse tree
	 */
	void enterObject(MKSONParser.ObjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#object}.
	 * @param ctx the parse tree
	 */
	void exitObject(MKSONParser.ObjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(MKSONParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(MKSONParser.PairContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArray(MKSONParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArray(MKSONParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(MKSONParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(MKSONParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(MKSONParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(MKSONParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(MKSONParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(MKSONParser.BoolContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#nulltype}.
	 * @param ctx the parse tree
	 */
	void enterNulltype(MKSONParser.NulltypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#nulltype}.
	 * @param ctx the parse tree
	 */
	void exitNulltype(MKSONParser.NulltypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MKSONParser#undefined}.
	 * @param ctx the parse tree
	 */
	void enterUndefined(MKSONParser.UndefinedContext ctx);
	/**
	 * Exit a parse tree produced by {@link MKSONParser#undefined}.
	 * @param ctx the parse tree
	 */
	void exitUndefined(MKSONParser.UndefinedContext ctx);
}