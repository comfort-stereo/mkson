
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import kotlin.system.exitProcess

fun evaluateRequirements(requirement: Any?, depth: Int = 1) : Boolean {

    val evaluate = { requirement: Any? ->
        if (evaluateRequirements(requirement, depth + 1)) {
            println("Passed")
            true
        } else {
            println("Failed")
            false
        }
    }

    when (requirement) {
        is Boolean -> return requirement
        is JSONArray -> {
            println()
            for ((i, subrequirement) in requirement.withIndex()) {
                print("    ".repeat(depth) + "Test $i -> ")
                if (!evaluate(subrequirement)) return false
            }
        }
        is JSONObject -> {
            for (subrequirement in requirement.keys()) {
                print("    ".repeat(depth) + "Testing $subrequirement -> ")
                if (!evaluate(requirement[subrequirement])) return false
            }
        }
    }

    return true
}

fun singleFileTest(test: String): String {
    val path = "src/test/rsc/" + test
    val input = path + "/input.mkson"
    val output = path + "/output.json"

    try {
        Compiler.main(arrayOf(input, output, "--indent", "4"), testing = true)
    } catch (exception: MksonError) {
        println("Compiler caught an error.")
        println("Failed")
        println()
        throw exception
    } catch (exception: Exception) {
        println("Compiler crashed with an unhandled internal error.")
        println("Failed")
        println()
        throw exception
    }

    val value = try {
        JSONArray("[" + File(output).readText() + "]").single()
    } catch (exception: JSONException) {
        return "Output JSON was not parsable."
    }

    if (value is JSONObject) {
        val requirements = try {
            value["requirements"] as? JSONObject
        } catch (exception: JSONException) {
            null
        } ?: return ""

        if (!evaluateRequirements(requirements)) return "Failed"
    }

    return ""
}

fun main(args: Array<String>) {

    val tests = listOf(
        "test_1_json_compatibility",
        "test_2_basic_syntax",
        "test_3_equality_and_truthyness",
        "test_4_numbers",
        "test_5_strings",
        "test_6_arrays",
        "test_7_objects",
        "test_8_inheritance",
        "test_9_references"
    )

    for (test in tests) {

        println("<<< Running '$test'... >>>")

        val errors = singleFileTest(test)

        if (errors.isEmpty()) {
            println("<<< Passed '$test' >>>")
            println()
            continue
        }

        println("<<< Failed '$test' >>>")
        println()
        println("A test was failed, exiting...")
        exitProcess(1)
    }

    println("All tests passed.")
}

